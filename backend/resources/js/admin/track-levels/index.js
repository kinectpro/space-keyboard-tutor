export class UsersIndex {
  constructor() {

    $(".delete").click((event) => {
      const $item = $(event.currentTarget);
      this.showConfirmationModal(
        $item.data('text-header-delete'),
        $item.data('text-body-delete'),
        $item.data('url')
      );
    });
  }

  showConfirmationModal(title, body, url) {
    const modalForm = $('#config-action-modal');

    modalForm.find('.modal-title').text(title);
    modalForm.find('.modal-body').text(body);
    modalForm.find('a.confirm').prop('href', url);

    modalForm.modal('show');
  }
}

$(function() {
  const usersIndex = new UsersIndex();
});