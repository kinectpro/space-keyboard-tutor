export class UsersIndex {
    constructor() {
        $(".ban-or-unban-user").click((event) => {
            const $item = $(event.currentTarget);
            if ($item.data('banned')) {
                this.showConfirmationModal(
                    $item.data(`text-header-${$item.data('banned') ? 'unban' : 'ban'}`),
                    $item.data(`text-body-${$item.data('banned') ? 'unban' : 'ban'}`),
                    $item.data('url')
                );
            } else {
                this.showBanUnbanModal(
                    $item.data(`text-header-${$item.data('banned') ? 'unban' : 'ban'}`),
                    $item.data(`text-body-${$item.data('banned') ? 'unban' : 'ban'}`),
                    $item.data('url')
                );
            };
        });

        $(".delete-or-restore-user").click((event) => {
            const $item = $(event.currentTarget);
            this.showConfirmationModal(
                $item.data(`text-header-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data(`text-body-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data('url')
            );
        });
    }

    showConfirmationModal(title, body, url) {
        const modalForm = $('#user-action-modal');

        modalForm.find('.modal-title').text(title);
        modalForm.find('.modal-body').text(body);
        modalForm.find('a.confirm').prop('href', url);

        modalForm.modal('show');
    }

    showBanUnbanModal(title, body, url) {
        const modalForm = $('#ban-unban-modal');

        modalForm.find('.modal-title').text(title);
        modalForm.find('form').prop('action', url);

        modalForm.modal('show');
    }
}

$(function() {
    const usersIndex = new UsersIndex();
});