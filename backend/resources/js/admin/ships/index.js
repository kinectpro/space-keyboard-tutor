export class ShipsIndex {
    constructor() {

        $(".delete-or-restore-ship").click((event) => {
            const $item = $(event.currentTarget);
            this.showConfirmationModal(
                $item.data(`text-header-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data(`text-body-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data('url')
            );
        });

        $(".unpublish-or-publish-ship").click((event) => {
            const $item = $(event.currentTarget);
            this.showConfirmationModal(
                $item.data(`text-header-${$item.data('unpublished') ? 'publish' : 'unpublish'}`),
                $item.data(`text-body-${$item.data('unpublished') ? 'publish' : 'unpublish'}`),
                $item.data('url')
            );
        });

        $(".disable-or-enable-ship").click((event) => {
            const $item = $(event.currentTarget);
            this.showConfirmationModal(
                $item.data(`text-header-${$item.data('disabled') ? 'enable' : 'disable'}`),
                $item.data(`text-body-${$item.data('disabled') ? 'enable' : 'disable'}`),
                $item.data('url')
            );
        });
    }

    showConfirmationModal(title, body, url) {
        const modalForm = $('#ship-action-modal');

        modalForm.find('.modal-title').text(title);
        modalForm.find('.modal-body').text(body);
        modalForm.find('a.confirm').prop('href', url);
        modalForm.modal('show');
    }

}

$(function() {
    const shipsIndex = new ShipsIndex();
});