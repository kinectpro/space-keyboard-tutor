export class LanguagesIndex {
    constructor() {

        $(".delete-or-restore-language").click((event) => {
            const $item = $(event.currentTarget);
            this.showConfirmationModal(
                $item.data(`text-header-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data(`text-body-${$item.data('deleted') ? 'restore' : 'delete'}`),
                $item.data('url')
            );
        });
    }

    showConfirmationModal(title, body, url) {
        const modalForm = $('#language-action-modal');

        modalForm.find('.modal-title').text(title);
        modalForm.find('.modal-body').text(body);
        modalForm.find('a.confirm').prop('href', url);
        modalForm.modal('show');
    }

}

$(function() {
    const languagesIndex = new LanguagesIndex();
});