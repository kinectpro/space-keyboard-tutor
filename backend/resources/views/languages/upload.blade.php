@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{route('languages.index')}}">Languages</a>
                </li>
                @if ($option_id != 0)
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('languages.view', $option_id)}}">
                            @foreach($languages as $language)
                                @if ($language->id == $option_id){{ $language->name }} @endif </a>
                        @endforeach
                    </li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">
                    Upload from file
                </li>
            </ol>
        </nav>

        {!! Form::open(['method' => 'POST','route' => 'languages.upload','files' => true]) !!}

        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')
                <div class="form-group row">
                    <label class="col-4 col-form-label text-right">{{ __('Language') }}</label>
                    <div class="col-4">
                        <select name="{{\App\Language::FIELD_ID}}" class="form-control" id="language_id">
                            @if ($option_id == 0)
                                <option value="">Select Language</option>
                            @endif
                            @foreach($languages as $language)
                                <option value="{{ $language->id }}"
                                        @if ($language->id == $option_id) selected @endif>{{ $language->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="dictionary" class="col-4 col-form-label text-right">{{ __('Word') }}</label>
                    <div class="col-4">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input btn-file" id="dictionary"
                                   aria-describedby="inputGroupFileAddon01"
                                   name='dictionary'>
                            <label class="custom-file-label" for="dictionary">Choose dictionary
                                file</label>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="card-footer text-right">
                <input type="submit" value="Submit" class="btn btn-success"/>
                <a class="btn btn-primary" href="{{ route('languages.index') }}" role="button">Cancel</a>
            </div>
        </div>
    </div>
@endsection
