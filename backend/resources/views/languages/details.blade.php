@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{route('languages.index')}}">Languages</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
{{--                    <a href="{{route('users.view', ['id' => $user->id])}}">{{$user->email}}</a>--}}
                    {{$language->name}}
                </li>
            </ol>
        </nav>

        <div class="card">
            {{--<div class="card-body">--}}

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="w-50">Property</th>
                    <th scope="col" class="w-50">Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ __('Id') }}</td>
                    <td>{{ $language->id }}</td>
                </tr>
                <tr>
                    <td>{{ __('Name') }}</td>
                    <td>{{$language->name}}</td>
                </tr>
                {{--<tr>--}}
                    {{--<td>{{ __('DeletedAt') }}</td>--}}
                    {{--<td class={{ $language->deleted_at ? '' : "text-danger" }}>{{ $language->deleted_at ? $language->deleted_at : 'None' }}</td>--}}
                {{--</tr>--}}
                </tbody>
            </table>

            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-success" href="{{ route('languages.edit', ['id' => $language->id]) }}" role="button">
                        <i class="fas fa-pen"></i> {{ __('Edit') }}
                    </a>
                    <a class="btn btn-primary" href="{{ route('languages.index') }}" role="button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
