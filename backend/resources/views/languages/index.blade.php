@extends('layouts.app')

@section('content')
    <div class="container">
        @include('shared.flash')

        <div class="table-responsive table-striped">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">@sortablelink('id')</th>
                    <th scope="col">@sortablelink('name')</th>
                    <th scope="col" class="text-right">Controls</th>
                </tr>
                </thead>


                <tbody>
                @foreach($languages as $language)
                    <tr @if ($language->deleted_at != null) class="text-danger" @endif>
                        <th scope="row">{{ $language->id }}</th>
                        <td>{{ $language->name }}</td>

                        <td class="text-right">
                            <a href="{{route('languages.view', $language->id)}}" title="View"
                               class="btn btn-outline-success">
                                <i class="far fa-eye"></i>
                            </a>

                            <a href="{{route('languages.from-file', $language->id)}}" title="Add Dictionary"
                               @if ($language->deleted_at != null) class="btn btn-outline-primary disabled"
                                @else class="btn btn-outline-primary" @endif >
                                <i class="fas fa-file-upload"></i>
                            </a>

                            <a href="{{route('languages.edit', $language->id)}}" title="Edit"
                               class="btn btn-outline-primary">
                                <i class="fa fa-edit"></i>
                            </a>

                            <a href="#" style="width: 40px"
                               title="{{ $language->deleted_at ? 'Restore' : 'Delete' }}"
                               class="delete-or-restore-language btn btn-outline-danger"
                               data-id="{{$language->id}}"
                               data-deleted="{{$language->deleted_at !== null}}"
                               data-text-header-delete="Delete language {{$language->name}}"
                               data-text-header-restore="'Restore language {{$language->name}}"
                               data-text-body-delete="Are you sure you want to delete this language?"
                               data-text-body-restore="Are you sure you want to restore this language?"
                               data-url='{{route('languages.delete', $language->id)}}'>
                                <i class="fas {{  $language->deleted_at ? 'fas fa-undo-alt' : 'fa-trash-alt'}}"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {!! $languages->appends(\Request::except('page'))->render() !!}
        </div>
    </div>

    <div class="modal fade" id="language-action-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <a class="btn btn-outline-danger confirm" href="#">Yes</a>
                    <a class="btn btn-outline-primary" data-dismiss="modal">No</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/admin/languages/index.js') }}" defer></script>
@endsection

