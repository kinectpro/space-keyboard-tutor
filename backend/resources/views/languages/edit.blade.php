@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('languages.index')}}">Languages</a>
                </li>
                @if (@isset ($language))
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{route('languages.view', $language->id)}}">{{$language->name}}</a>
                </li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">
                    @if (@isset ($language)) {{ 'Edit' }} @else {{'New Language'}} @endif
                </li>
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                @include('shared.alerts')

                {!! Form::model(isset($language) ? $language : \App\Language::class,[
                    'route' => ['languages.update', isset($language) ? $language->id : null],
                    'id' => 'form'
                ]) !!}

                <div class="form-group row">
                    <label for="nickname" class="col-4 col-form-label text-right">{{ __('Language name') }}</label>
                    <div class="col-4">
                        {!! Form::text(\App\Language::FIELD_NAME, null, [
                            'class' => 'form-control',
                            'autofocus' => true,
                            'required' => true
                        ]) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="card-footer text-right">
                {!! Form::submit(__('Save'), ['class' => 'btn btn-success', 'form' => 'form']); !!}
                <a class="btn btn-primary" href="{{ route('languages.index') }}" role="button">Cancel</a>
            </div>
        </div>
    </div>
@endsection
