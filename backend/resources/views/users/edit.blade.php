@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('users.index')}}">Users</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('users.view', ['id' => $user->id])}}">{{$user->email}}</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Edit
                </li>
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                @include('shared.alerts')

                {!! Form::model($user, ['route' => ['users.update', $user->id], 'id' => 'form']) !!}
                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Email') }}</label>
                        <div class="col-md-8">
                            {!! Form::text(\App\User::FIELD_EMAIL, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <div class="col-md-8">
                            {!! Form::password(\App\User::FIELD_PASSWORD, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nickname" class="col-sm-4 col-form-label text-md-right">{{ __('NickName') }}</label>
                        <div class="col-md-8">
                            {!! Form::text(\App\User::FIELD_NICKNAME, null, [
                                'class' => 'form-control',
                                'autofocus' => true,
                                'required' => true
                            ]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="experience" class="col-sm-4 col-form-label text-md-right">{{ __('Experience') }}</label>
                        <div class="col-md-8">
                            {!! Form::text(\App\User::FIELD_EXPERIENCE, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dock_size" class="col-sm-4 col-form-label text-md-right">{{ __('DockSize') }}</label>
                        <div class="col-md-8">
                            {!! Form::text(\App\User::FIELD_DOCK_SIZE, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="card-footer text-right">
                {!! Form::submit(__('Update'), ['class' => 'btn btn-success', 'form' => 'form']); !!}
                <a class="btn btn-primary" href="{{ route('users.index') }}" role="button">Cancel</a>
            </div>
        </div>
    </div>
@endsection
