@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @include('shared.flash')

        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">
                        <form>
                            <input type="search" id="filter_email" name="filter_email" class="form-control"
                                   placeholder="Filter by email" value="{{ Request::input('filter_email') }}"
                            >
                        </form>
                    </th>
                    <th scope="col">
                        <form>
                            <input type="search" id="filter_nickname" name="filter_nickname" class="form-control"
                                   placeholder="Filter by nickname" value="{{ Request::input('filter_nickname') }}"
                            >
                        </form>
                    </th>
                    <th scope="col" colspan="7"></th>
                </tr>

                <tr>
                    <th scope="col">@sortablelink('id')</th>
                    <th scope="col">@sortablelink('email')</th>
                    <th scope="col">@sortablelink('nickname')</th>
                    <th scope="col">@sortablelink('level')</th>
                    <th scope="col">@sortablelink('experience')</th>
                    <th scope="col">@sortablelink('dock_size', 'Dock size')</th>
                    <th scope="col">@sortablelink('confirmed_at', 'Confirmed')</th>
                    <th scope="col">@sortablelink('banned_at', 'Banned')</th>
                    <th scope="col">@sortablelink('unban_at', 'Unban')</th>
                    <th scope="col">@sortablelink('deleted_at', 'Deleted')</th>
                    <th scope="col" class="text-right">Controls</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr @if($user->deleted_at != null) class="alert-danger" @endif>
                        <th scope="row">{{ $user->id }}</th>
                        <td>
                            <a href="{{route('users.view', $user->id)}}" title="View">{{ $user->email }}</a>
                        </td>
                        <td>{{ $user->nickname }}</td>
                        <td>{{ $user->level() }}</td>
                        <td>{{ $user->experience }}</td>
                        <td>{{ $user->dock_size }}</td>
                        <td>{{ $user->confirmed_at }}</td>
                        <td>{{ $user->banned_at }}</td>
                        <td>{{ $user->unban_at }}</td>
                        <td>{{ $user->deleted_at }}</td>

                        <td class="text-right">
                            <a href="{{route('users.view', $user->id)}}" title="View" class="btn btn-outline-primary">
                                <i class="far fa-eye"></i>
                            </a>

                            <a href="{{route('users.edit', $user->id)}}" title="Edit" class="btn btn-outline-primary">
                                <i class="fa fa-edit"></i>
                            </a>

                            <a href="#" title="{{$user->banned_at ? 'Unban' : 'Ban'}}"
                               class="ban-or-unban-user btn btn-outline-primary"
                               data-id="{{$user->id}}"
                               data-banned="{{$user->banned_at !== null}}"
                               data-text-header-ban="Ban user"
                               data-text-header-unban="Unban user"
                               data-text-body-ban="Are you sure you want to ban this user?"
                               data-text-body-unban="Are you sure you want to unban this user?"
                               data-url='{{route('users.ban-or-unban', array_merge([$user->id], request()->query()))}}'
                            >
                                <i class="fas {{ $user->banned_at ? 'fa-user-check' : 'fa-user-slash'}}"></i>
                            </a>

                            <a href="#" title="{{ $user->deleted_at ? 'Restore' : 'Delete' }}"
                               class="delete-or-restore-user btn btn-outline-danger"
                               data-id="{{$user->id}}"
                               data-deleted="{{$user->deleted_at !== null}}"
                               data-text-header-delete="Delete user"
                               data-text-header-restore="'Restore user"
                               data-text-body-delete="Are you sure you want to delete this user?"
                               data-text-body-restore="Are you sure you want to restore this user?"
                               data-url='{{route('users.delete-or-restore', array_merge([$user->id], request()->query()))}}'
                            >
                                <i class="fas {{  $user->deleted_at ? 'fa-church' : 'fa-trash-alt'}}"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {!! $users->appends(\Request::except('page'))->render() !!}
        </div>
    </div>

    <div class="modal fade" id="user-action-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body"></div>

                <div class="modal-footer">
                    <a type="button" class="btn btn-outline-danger confirm" href="#">Yes</a>

                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ban-unban-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form" id="ban-options-form">
                        <div class="form-group">
                            <label for="banUpTo" class="col-form-label">Ban up to</label>
                            <input type="date" class="form-control" name="banUpTo">
                        </div>
                        <div class="form-group">
                            <label for="banPeriod">Interval</label>
                            <select class="form-control" name="banPeriod">
                                <option value="5" selected>5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="">Forever</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="page" value="{!! $users->currentPage() !!}">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-danger" form="ban-options-form">Yes</button>
                    <button class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/admin/users/index.js') }}" defer></script>
@endsection


