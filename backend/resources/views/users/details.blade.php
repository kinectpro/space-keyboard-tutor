@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{route('users.index')}}">Users</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{route('users.view', ['id' => $user->id])}}">{{$user->email}}</a>
                </li>
            </ol>
        </nav>

        <div class="card">
            {{--<div class="card-body">--}}

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="w-50">Property</th>
                    <th scope="col" class="w-50">Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ __('Email') }}</td>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <td>{{ __('NickName') }}</td>
                    <td>{{$user->nickname}}</td>
                </tr>
                <tr>
                    <td>{{ __('Level') }}</td>
                    <td>{{$user->level()}}</td>
                </tr>
                <tr>
                    <td>{{ __('Experience') }}</td>
                    <td>{{$user->experience}}</td>
                </tr>
                <tr>
                    <td>{{ __('DockSize') }}</td>
                    <td>{{$user->dock_size}}</td>
                </tr>
                <tr>
                    <td>{{ __('ConfirmedAt') }}</td>
                    <td class={{ $user->confirmed_at ? '' : "text-danger" }}>{{$user->confirmed_at ? $user->confirmed_at : 'None' }}</td>
                </tr>
                <tr>
                    <td>{{ __('BannedAt') }}</td>
                    <td class={{ $user->banned_at ? '' : "text-danger" }}>{{$user->banned_at ? $user->banned_at : 'None' }}</td>
                </tr>
                <tr>
                    <td>{{ __('UnbanAt') }}</td>
                    <td class={{ $user->unban_at ? '' : "text-danger" }}>{{$user->unban_at ? $user->unban_at : 'None' }}</td>
                </tr>
                <tr>
                    <td>{{ __('DeletedAt') }}</td>
                    <td class={{ $user->deleted_at ? '' : "text-danger" }}>{{ $user->deleted_at ? $user->deleted_at : 'None' }}</td>
                </tr>
                </tbody>
            </table>


            {{--<div class="form-group row">--}}
            {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Email') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="email" type="email"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="email" value="{{ $user->email }}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="nickname" class="col-sm-4 col-form-label text-md-right">{{ __('NickName') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="nickname" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="nickname" value="{{$user->nickname}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="experience" class="col-sm-4 col-form-label text-md-right">{{ __('Experience') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="experience" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="experience" value="{{$user->experience}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="dock_size" class="col-sm-4 col-form-label text-md-right">{{ __('DockSize') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="dock_size" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="dock_size" value="{{$user->dock_size}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="Confirmed_at"--}}
            {{--class="col-sm-4 col-form-label text-md-right">{{ __('ConfirmedAt') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="Confirmed_at" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="Confirmed_at" value="{{$user->confirmed_at}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="Banned_at" class="col-sm-4 col-form-label text-md-right">{{ __('BannedAt') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="Banned_at" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="Banned_at" value="{{$user->banned_at}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="Unban_at" class="col-sm-4 col-form-label text-md-right">{{ __('UnbanAt') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="Unban_at" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="Unban_at" value="{{$user->unban_at}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group row">--}}
            {{--<label for="Deleted_at" class="col-sm-4 col-form-label text-md-right">{{ __('DeletedAt') }}</label>--}}
            {{--<div class="col-md-8">--}}
            {{--<input id="Deleted_at" type="text"--}}
            {{--class="form-control-plaintext"--}}
            {{--name="Deleted_at" value="{{$user->deleted_at}}" readonly>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-success" href="{{ route('users.edit', ['id' => $user->id]) }}" role="button">
                        <i class="fas fa-pen"></i> {{ __('Edit') }}
                    </a>
                    <a class="btn btn-primary" href="{{ route('users.index') }}" role="button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
