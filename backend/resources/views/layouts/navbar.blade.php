<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'Admin') }}
    </a>

    <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @else
                <li class="nav-item @if (Request::is('/*')) active @endif">
                    <a class="nav-link" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                </li>

                <li class="nav-item @if (Request::is('users*')) active @endif">
                    <a class="nav-link" href="{{ route('users.index') }}">{{ __('Users') }}</a>
                <li class="nav-item">
                    <div class="dropdown">
                        <span class="sr-only"></span>
                    </div>
                </li>

                <li class="nav-item dropdown @if (Request::is('lang*','dict*')) active @endif">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        Dictionary
                    </a>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('languages.index') }}" role="button">Languages</a>
                        <a class="dropdown-item" href="{{ route('languages.new') }}" role="button">New language</a>
                        <a class="dropdown-item" href="{{ route('languages.from-file')}}" role="button">Import dictionary</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('dictionary.index') }}">Dictionary</a>
                        <a class="dropdown-item" href="{{ route('dictionary.new') }}" role="button">New Word</a>
                    </div>
                </li>


                <li class="nav-item dropdown @if (Request::is('ships/*')) active @endif">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        Ships
                    </a>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('ships.index') }}" role="button">List</a>
                        <a class="dropdown-item" href="{{ route('ships.new') }}">New</a>
                    </div>
                </li>

                <li class="nav-item dropdown @if (Request::is('track-levels*')) active @endif">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Track levels
                    </a>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('track-levels.index') }}" role="button">All levels</a>
                        <a class="dropdown-item" href="{{ route('track-levels.new') }}" role="button">New level</a>
                    </div>
                </li>

                <li class="nav-item dropdown @if (Request::is('levels*')) active @endif">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        Difficulty
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('levels.index') }}" role="button">Levels</a>
                        <a class="dropdown-item" href="{{ route('levels.new') }}" role="button">New level</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </li>
            @endguest

        </ul>
    </div>
</nav>
