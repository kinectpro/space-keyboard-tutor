@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('levels.index')}}">Levels</a>
                </li>
                @if (@isset ($level))
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('levels.view', $level->id)}}">{{$level->level}}</a>
                    </li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">
                    @if (@isset ($level)) {{ 'Edit' }} @else {{'New Level'}} @endif
                </li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')

                {!! Form::model(isset($level) ? $level : \App\Level::class,[
                    'route' => ['levels.save', isset($level) ? $level->id : null],
                    'id' => 'form'
                ]) !!}

                <div class="form-group row">
                    <label for="level" class="col-4 col-form-label">{{ __('Level') }}</label>
                    <div class="col-4">
                        {!! Form::number('level', null, ['class' => 'form-control', 'autofocus' => true,'required' => true]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="experience" class="col-4 col-form-label">{{ __('Experience') }}</label>
                    <div class="col-4">
                        {!! Form::number('experience', null, ['class' => 'form-control', 'autofocus' => true,'required' => true]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="min_length" class="col-4 col-form-label">{{ __('Minimum Word Length') }}</label>
                    <div class="col-4">
                        {!! Form::number('min_length', null, ['class' => 'form-control', 'autofocus' => true,'required' => true]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="max_length" class="col-4 col-form-label">{{ __('Maximum Word Length') }}</label>
                    <div class="col-4">
                        {!! Form::number('max_length', null, ['class' => 'form-control', 'autofocus' => true,'required' => true]) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>

            <div class="card-footer text-right">
                {!! Form::submit(__('Save'), ['class' => 'btn btn-success', 'form' => 'form']); !!}
                <a class="btn btn-primary" href="{{ route('levels.index') }}" role="button">Cancel</a>
            </div>
        </div>
    </div>
@endsection