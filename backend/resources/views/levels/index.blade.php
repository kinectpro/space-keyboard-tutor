@extends('layouts.app')
@section('content')
    <div class="container">
        @include('shared.flash')
        @include('shared.alerts')
        <div class="table-responsive table-striped">
        <table class="table table-bordered">
            <thead class="thread-light">
            <tr>
                <th scope="col">@sortablelink('id')</th>
                <th scope="col">@sortablelink('level')</th>
                <th scope="col">@sortablelink('experience')</th>
                <th scope="col">@sortablelink('min_length','Minimum word length')</th>
                <th scope="col">@sortablelink('max_length','Maximum word length')</th>
                <th scope="col" class="text-right" style="min-width: 160px">Controls</th>
            </tr>
            </thead>
            <tbody>
            @foreach($levels as $level)
                <tr>
                    <td class="align-middle">{{ $level->id }}</td>
                    <td class="align-middle">{{ $level->level }}</td>
                    <td class="align-middle">{{ $level->experience }}</td>
                    <td class="align-middle">{{ $level->min_length }}</td>
                    <td class="align-middle">{{ $level->max_length }}</td>
                    <td class="text-right align-middle">
                        <a class="btn btn-outline-success" href="{{ route('levels.view', $level->id) }}" title="View">
                            <i class="far fa-eye"></i>
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('levels.edit', $level->id) }}" title="Edit">
                            <i class="far fa-edit"></i>
                        </a>
                        <a href="{{route('levels.delete', $level->id)}}"
                           onclick="return confirm('Are you sure you want to delete level: {{$level->level}} ?')"
                           title="Delete"
                           class="btn btn-outline-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
        <div class="row justify-content-center">
            {!! $levels->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
@endsection
