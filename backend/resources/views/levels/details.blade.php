@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('levels.index')}}">Levels</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{$level->level}}
                </li>
                </li>
            </ol>
        </nav>

        <div class="card">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="w-50">Property</th>
                    <th scope="col" class="w-50">Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ __('Id') }}</td>
                    <td>{{ $level->id }}</td>
                </tr>
                <tr>
                    <td>{{ __('Language') }}</td>
                    <td>{{ $level->level }}</td>
                </tr>
                <tr>
                    <td>{{ __('Experience') }}</td>
                    <td>{{ $level->experience }}</td>
                </tr>
                <tr>
                    <td>{{ __('Minimum Word Length') }}</td>
                    <td>{{ $level->min_length }}</td>
                </tr>
                <tr>
                    <td>{{ __('Maximum Word Length') }}</td>
                    <td>{{ $level->max_length }}</td>
                </tr>
                </tbody>
            </table>

            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-success" href="{{ route('levels.edit', ['id' => $level->id]) }}"
                       role="button">
                        <i class="fas fa-pen"></i> {{ __('Edit') }}
                    </a>
                    <a class="btn btn-primary" href="{{ route('levels.index') }}" role="button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
