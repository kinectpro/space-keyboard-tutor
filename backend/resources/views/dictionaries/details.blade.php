@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{route('languages.view', $word->language->id)}}">{{$word->language->name}}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('dictionary.index', [\App\Word::FIELD_LANGUAGE_ID => $word->language->id])}}">Words</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{$word->word}}
                </li>
                </li>
            </ol>
        </nav>

        <div class="card">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="w-50">Property</th>
                    <th scope="col" class="w-50">Value</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ __('Id') }}</td>
                    <td>{{ $word->id }}</td>
                </tr>
                <tr>
                    <td>{{ __('Language') }}</td>
                    <td>{{ $word->language->name }}</td>
                </tr>
                <tr>
                    <td>{{ __('Word') }}</td>
                    <td>{{ $word->word }}</td>
                </tr>
                </tbody>
            </table>

            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-success" href="{{ route('dictionary.edit', ['id' => $word->id]) }}"
                       role="button">
                        <i class="fas fa-pen"></i> {{ __('Edit') }}
                    </a>
                    <a class="btn btn-primary" href="{{ route('dictionary.index') }}" role="button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
