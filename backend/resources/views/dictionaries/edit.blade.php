@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                @if (@isset ($word))
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('languages.view', $word->language->id)}}">{{$word->language->name}}</a>
                    </li>
                @endif
                <li class="breadcrumb-item">
                    <a href="{{route('dictionary.index')}}">Words</a>
                </li>
                @if (@isset ($word))
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('dictionary.view', $word->id)}}">{{$word->word}}</a>
                    </li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">
                    @if (@isset ($word)) {{ 'Edit' }} @else {{'New Word'}} @endif
                </li>
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')

                {!! Form::model(isset($word) ? $word : \App\Word::class,[
                    'route' => ['dictionary.update', isset($word) ? $word->id : null],
                    'id' => 'form'
                ]) !!}

                <div class="form-group row">
                    <label class="col-4 col-form-label">{{ __('Language') }}</label>
                    <div class="col-4">
                        <select name="{{ \App\Word::FIELD_LANGUAGE_ID }}" class="form-control">
                            @if (!@isset ($word))
                                <option value="" @if (!$language_id) selected @endif>Select Language</option>
                            @endif

                            @foreach($languages as $language)
                                <option value="{{ $language->id }}" @if ($language->id == $language_id) selected @endif>
                                    {{ $language->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="word" class="col-4 col-form-label">{{ __('Word') }}</label>
                    <div class="col-4">
                        {!! Form::text('word', null, ['class' => 'form-control', 'autofocus' => true,'required' => true]) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="card-footer text-right">
                {!! Form::submit(__('Save'), ['class' => 'btn btn-success', 'form' => 'form']); !!}
                <a class="btn btn-primary" href="{{ route('dictionary.index') }}" role="button">Cancel</a>
            </div>
        </div>
    </div>
@endsection
