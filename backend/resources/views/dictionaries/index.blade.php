@extends('layouts.app')

@section('content')
    <div class="container">
        @include('shared.flash')

        <form name="dictForm" action="{{route('dictionary.index', "language_id")}}" onchange="document.forms['dictForm'].submit()">
            <div class="form-row align-items-center">
                <div class="col-auto my-1">
                    <label class="mr-sm-2" for="language_id">Language</label>
                </div>
                <div class="col-auto my-1">
                    <select class="custom-select custom-select-lg mb-3" id="language_id" name="language_id">
                        <option value="" @if (!$option_id) selected @endif>All</option>
                        @foreach($languages as $language)
                            <option value="{{ $language->id }}"@if ($language->id == $option_id) selected @endif >
                                {{ $language->name }} @if ($language->deleted_at) (Deleted) @endif
                            </option>
                        @endforeach
                    </select >
                </div>
            </div>
        </form >

        <div class="table-responsive table-striped">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">@sortablelink('id')</th>
                    <th scope="col">@sortablelink('language')</th>
                    <th scope="col">@sortablelink('word')</th>
                    <th scope="col" class="text-right">Controls</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dictionary as $word)
                    <tr @if ($word->language->deleted_at != null) class="text-danger" @endif>
                        <th scope="row">{{ $word->id }}</th>
                        <td scope="row">{{ $word->language->name }}</td>
                        <td scope="row">{{ $word->word }}</td>

                        <td class="text-right">
                            <a href="{{route('dictionary.view', $word->id)}}" title="View" class="btn btn-outline-success"
                               class="btn btn-outline-primary">
                                <i class="far fa-eye"></i>
                            </a>

                            <a href="{{route('dictionary.edit', $word->id)}}" title="Edit" class="btn btn-outline-primary"
                               class="btn btn-outline-primary">
                                <i class="fa fa-edit"></i>
                            </a>

                            <a href="{{route('dictionary.delete', $word->id)}}"
                               onclick="return confirm('Are you sure you want to delete word: {{$word->word}} ?')"
                               title="Delete"
                               class="btn btn-outline-danger">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {!! $dictionary->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/admin/dictionaries/index.js') }}" defer></script>
@endsection

