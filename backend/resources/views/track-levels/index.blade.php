@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        @include('shared.flash')
        @include('shared.alerts')

        <table class="table table-bordered table-striped">
            <thead class="thread-light">
            <tr>
                <th scope="col">@sortablelink('id')</th>
                <th scope="col">@sortablelink('track_level')</th>
                <th scope="col">@sortablelink('min_level_word')</th>
                <th scope="col">@sortablelink('max_level_word')</th>
                <th scope="col" class="text-right" style="min-width: 160px">Controls</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trackLevels as $trackLevel)
                <tr>
                    <td>{{ $trackLevel->id }}</td>
                    <td>{{ $trackLevel->track_level }}</td>
                    <td>{{ $trackLevel->min_level_word }}</td>
                    <td>{{ $trackLevel->max_level_word }}</td>
                    <td class="text-right">

                        <a class="btn btn-outline-primary" href="{{ route('track-levels.edit', $trackLevel->id) }}" title="Edit">
                            <i class="far fa-edit"></i>
                        </a>

                        <a class="btn btn-outline-danger delete" title="Delete"
                           data-text-header-delete="Delete"
                           data-text-body-delete="Are you sure you want to delete this configuration?"
                           data-url='{{route('track-levels.delete', $trackLevel->id) }}'
                        >
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row justify-content-center">
            {{ $trackLevels->links() }}
        </div>
    </div>

    <div class="modal fade" id="config-action-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body"></div>

                <div class="modal-footer">
                    <a type="button" class="btn btn-outline-danger confirm" href="#">Yes</a>

                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/track-levels/index.js') }}" defer></script>
@endsection
