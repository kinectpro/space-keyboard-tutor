@extends('layouts.app')
@section('content')
    <div class="container" style="width: fit-content">

        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')

                {!! Form::model($trackLevel, [
                'method' => 'POST',
                'route' => ['track-levels.save', $trackLevel->id],
                ])!!}

                @include('track-levels._form')

                <div class="card-footer text-right">
                    {!! Form::submit(__('Save'), ['class' => 'btn btn-success']); !!}

                    <a class="btn btn-primary" href="{{ route('track-levels.index') }}" role="button">Cancel</a>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
