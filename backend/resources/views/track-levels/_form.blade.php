<div class="form-group row">
    <label for="track_level" class="col-sm-4 col-form-label text-md-right">{{ __('Level') }}</label>
    <div class="col-md-8">
        {!! Form::number('track_level', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="min_level_words" class="col-sm-4 col-form-label text-md-right">{{ __('Min level words') }}</label>
    <div class="col-md-8">
        {!! Form::number('min_level_word', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="max_level_words" class="col-sm-4 col-form-label text-md-right">{{ __('Max level words') }}</label>
    <div class="col-md-8">
        {!! Form::number('max_level_word', null, ['class' => 'form-control']) !!}
    </div>
</div>
