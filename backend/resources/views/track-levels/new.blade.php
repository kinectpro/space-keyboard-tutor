@extends('layouts.app')
@section('content')
    <div class="container" style="width: fit-content">
        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')

                {!! Form::open([
                'method' => 'POST',
                'route' => 'track-levels.save',
                'files' => true
                ]) !!}

                @include('track-levels._form')

                <div class="card-footer text-right">
                    {!! Form::submit(__('Create'), ['class' => 'btn btn-success']); !!}
                    <a class="btn btn-primary" href="{{ route('track-levels.index') }}" role="button">Cancel</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
