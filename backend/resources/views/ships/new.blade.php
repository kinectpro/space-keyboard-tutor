@extends('layouts.app')
@section('content')
    <div class="container" style="width: fit-content">


        {!! Form::open([
        'method' => 'POST',
        'route' => 'ships.save',
        'files' => true
        ]) !!}

        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')
                @include('ships._form')

                <div class="form-group row">
                    <label for="image" class="col-sm-4 col-form-label text-md-right">{{ __('Image') }}</label>
                    <div class="col-sm-4">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input btn-file" id="image"
                                   aria-describedby="inputGroupFileAddon01"
                                   name='image'>
                            <label class="custom-file-label" style="width: 210%" for="image">Choose ship image</label>
                        </div>
                        <img style="max-height: 200px; max-width: 200px; margin: 10px" id='img-upload'/>
                        @include('shared.image-upload')
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="card-footer text-right">
                    <a class="btn btn-success" href="{{ route('ships.index') }}" title="Cancel">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    {!! Form::submit('Create', ['class' => 'btn btn-primary', 'title' => 'Create']) !!}
                </div>

            </div>
        </div>
    </div>
@endsection