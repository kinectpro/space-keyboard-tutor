@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        @include('shared.flash')
        @include('shared.alerts')
        <div class="table-responsive table-striped">
        <table class="table table-bordered">
            <thead class="thread-light">
            <tr>
                <th scope="col">@sortablelink('id')</th>
                <th scope="col">@sortablelink('level')</th>
                <th scope="col">@sortablelink('name')</th>
                <th scope="col">@sortablelink('health')</th>
                <th scope="col">@sortablelink('fuel')</th>
                <th scope="col">@sortablelink('silver', 'Price in Silver')</th>
                <th scope="col">@sortablelink('gold', 'Price in Gold')</th>
                <th scope="col">Image</th>
                <th scope="col">@sortablelink('is_published', 'Published')</th>
                <th scope="col">@sortablelink('is_enabled', 'Enabled')</th>
                <th scope="col" class="text-right" style="min-width: 160px">Controls</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ships as $ship)
                <tr @if ($ship->deleted_at != null) class="alert-danger" @endif>
                    <td class="align-middle">{{ $ship->id }}</td>
                    <td class="align-middle">{{ $ship->level }}</td>
                    <td class="align-middle">{{ $ship->name }}</td>
                    <td class="align-middle">{{ $ship->health }}</td>
                    <td class="align-middle">{{ $ship->fuel }}</td>
                    <td class="align-middle">{{ $ship->silver }}</td>
                    <td class="align-middle">{{ $ship->gold }}</td>
                    <td class="align-middle"><img
                        @if ($ship->image != null) src="{{ url('storage/image/ships/'.$ship->image) }}"
                        @else src="{{url('/images/noimage.png')}}"
                        @endif
                        style="max-height: 50px; max-width: 50px"></td>
                    <td class="align-middle {{ $ship->is_published ? 'text-success' : "text-danger" }}">{{$ship->is_published ? 'Yes' : 'No' }}</td>
                    <td class="align-middle {{ $ship->is_enabled ? 'text-success' : "text-danger" }}">{{$ship->is_enabled ? 'Yes' : 'No' }}</td>
                    <td class="text-right align-middle">
                        <a class="btn btn-outline-success" href="{{ route('ships.view', $ship->id) }}" title="View">
                            <i class="far fa-eye"></i>
                        </a>

                        <a class="btn btn-outline-primary" href="{{ route('ships.edit', $ship->id) }}" title="Edit">
                            <i class="far fa-edit"></i>
                        </a>

                        <a style="width: 40px" href="#" title="{{($ship->is_published == false) ? 'Publish' : 'Unpublish' }}" class="btn btn-outline-primary unpublish-or-publish-ship"
                           data-id="{{$ship->id}}"
                           data-unpublished="{{$ship->is_published == false}}"
                           data-text-header-unpublish="Unpublish ship"
                           data-text-header-publish="Publish ship"
                           data-text-body-unpublish="Are you sure you want to unpublish this ship?"
                           data-text-body-publish="Are you sure you want to publish this ship?"
                           data-url='{{route('ships.publish', $ship->id)}}'>
                            <i class="fas {{  ($ship->is_published == false) ? 'far fa-calendar-plus' : 'far fa-calendar-minus'}}"></i>
                        </a>

                        <a style="width: 40px" href="#" title="{{ ($ship->is_enabled == false) ? 'Enable' : 'Disable' }}" class="btn btn-outline-primary disable-or-enable-ship"
                           data-id="{{$ship->id}}"
                           data-disabled="{{$ship->is_enabled == false}}"
                           data-text-header-disable="Disable ship"
                           data-text-header-enable="Enable ship"
                           data-text-body-disable="Are you sure you want to disable this ship?"
                           data-text-body-enable="Are you sure you want to enable this ship?"
                           data-url='{{route('ships.enable', $ship->id)}}'>
                            <i class="fas {{  ($ship->is_enabled == false) ? 'fas fa-toggle-off' : 'fas fa-toggle-on'}}"></i>
                        </a>

                        <a style="width: 40px" href="#" title="{{ $ship->deleted_at ? 'Restore' : 'Delete' }}" class="btn btn-outline-danger delete-or-restore-ship"
                           data-id="{{$ship->id}}"
                           data-deleted="{{$ship->deleted_at !== null}}"
                           data-text-header-delete="Delete ship"
                           data-text-header-restore="Restore ship"
                           data-text-body-delete="Are you sure you want to delete this ship?"
                           data-text-body-restore="Are you sure you want to restore this ship?"
                           data-url='{{route('ships.delete', $ship->id)}}'>
                            <i class="fas {{  $ship->deleted_at ? 'fas fa-undo-alt' : 'fa-trash-alt'}}"></i>
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
        <div class="row justify-content-center">
            {!! $ships->appends(\Request::except('page'))->render() !!}
        </div>

    </div>

    <div class="modal fade" id="ship-action-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <a class="btn btn-outline-danger confirm" href="#">Yes</a>
                    <a class="btn btn-outline-primary" href="#" data-dismiss="modal">No</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/ships/index.js') }}" defer></script>
@endsection
