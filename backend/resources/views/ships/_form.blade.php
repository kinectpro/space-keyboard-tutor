<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Name') }}</label>
    <div class="col-md-8">
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="level" class="col-sm-4 col-form-label text-md-right">{{ __('Level') }}</label>
    <div class="col-md-8">
    {!! Form::number('level', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="health" class="col-sm-4 col-form-label text-md-right">{{ __('Health') }}</label>
    <div class="col-md-8">
    {!! Form::number('health', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="fuel" class="col-sm-4 col-form-label text-md-right">{{ __('Fuel') }}</label>
    <div class="col-md-8">
    {!! Form::number('fuel', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="silver" class="col-sm-4 col-form-label text-md-right">{{ __('Price in Silver') }}</label>
    <div class="col-md-8">
    {!! Form::number('silver', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    <label for="gold" class="col-sm-4 col-form-label text-md-right">{{ __('Price in Gold') }}</label>
    <div class="col-md-8">
        {!! Form::number('gold', null, ['class' => 'form-control']) !!}
    </div>
</div>
{{--
<div class="form-group row">
    <label for="image" class="col-sm-4 col-form-label text-md-right">{{ __('Image') }}</label>--}}
