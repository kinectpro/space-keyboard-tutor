@extends('layouts.app')
@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{route('ships.index')}}">Ships</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{$ship->name}} - ({{$ship->id}})</li>
            </ol>
        </nav>
        <div class="card">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Property</th>
                    <th scope="col">Value</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>Id</td>
                    <td>{{ $ship->id }}</td>
                </tr>

                <tr>
                    <td>Name</td>
                    <td>{{ $ship->name }}</td>
                </tr>

                <tr>
                    <td>Level</td>
                    <td>{{ $ship->level }}</td>
                </tr>

                <tr>
                    <td>Helth</td>
                    <td>{{ $ship->health }}</td>
                </tr>

                <tr>
                    <td>Fuel</td>
                    <td>{{ $ship->fuel }}</td>
                </tr>

                <tr>
                    <td>Price in Silver</td>
                    <td>{{ $ship->silver }}</td>
                </tr>

                <tr>
                    <td>Price in Gold</td>
                    <td>{{ $ship->gold }}</td>
                </tr>

                <tr>
                    <td>Creation date</td>
                    <td>{{ $ship->created_at }}</td>
                </tr>

                <tr>
                    <td>Last modification date</td>
                    <td>{{ $ship->updated_at }}</td>
                </tr>

                <tr>
                    <td>Published</td>
                    <td class="{{ $ship->is_published ? 'text-success' : "text-danger" }}">{{$ship->is_published ? 'Yes' : 'No' }}</td>
                </tr>

                <tr>
                    <td>Enabled</td>
                    <td class="{{ $ship->is_enabled ? 'text-success' : "text-danger" }}">{{$ship->is_enabled ? 'Yes' : 'No' }}</td>
                </tr>

                <tr>
                    <td>Image</td>
                    <td>
                        <img src="{{ asset($ship->image != null ? ('storage/image/ships/'.$ship->image) : '/images/noimage.png') }}"
                             class="admin-view-ship-img"
                        >
                    </td>
                </tr>

                </tbody>
            </table>

            <div class="card-footer">
                <p class="text-right">
                    <a class="btn btn-outline-success" href="{{ route('ships.index') }}" title="Back">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-outline-primary" href="{{ route('ships.edit', $ship->id) }}" title="Edit">
                        <i class="far fa-edit"></i>
                    </a>
                    <a class="btn btn-outline-danger" onclick="return confirm('Are you sure?')"
                       href="{{ route('ships.delete', $ship->id) }}" title="Delete">
                        <i class="fa fa-times"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
@endsection