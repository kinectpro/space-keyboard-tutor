@extends('layouts.app')
@section('content')
    <div class="container" style="width: fit-content">


        {!! Form::model($ship, [
        'method' => 'POST',
        'route' => ['ships.save', $ship->id],
        'files' => true
        ])!!}

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('ships.index')}}">Ships</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{route('ships.view', ['id' => $ship->id])}}">{{$ship->name}}</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Edit
                </li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-body">
                @include('shared.flash')
                @include('shared.alerts')
                @include('ships._form')
                <div class="form-group row">
                    <label for="image" class="col-sm-4 col-form-label text-md-right">{{ __('Image') }}</label>
                    <div class="col-sm-4">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input btn-file " id="image"
                                   aria-describedby="inputGroupFileAddon01"
                                   name='image'>
                            <label class="custom-file-label" style="width: 210%" for="image">{{ $ship->image }}</label>
                        </div>
                        <img
                                @if ($ship->image != null) src="{{ url('storage/image/ships/'.$ship->image) }}"
                                @else src="{{url('/images/noimage.png')}}"
                                @endif
                                style="max-height: 200px; max-width: 200px; margin: 10px" id='img-upload'/>
                        @include('shared.image-upload')
                    </div>

                    {!! Form::close() !!}
                </div>

                <div class="card-footer text-right">
                    <a class="btn btn-success" href="{{ route('ships.index') }}" title="Back">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    {!! Form::submit('Save', ['class' => 'btn btn-primary', 'title' => 'Save']) !!}
                </div>

            </div>
        </div>
    </div>
@endsection