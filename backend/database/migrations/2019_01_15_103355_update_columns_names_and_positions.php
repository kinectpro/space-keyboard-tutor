<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsNamesAndPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::table('statistics', function (Blueprint $table) {
                $table->dropColumn('period');
                });

//        DB::statement("ALTER TABLE race_results MODIFY COLUMN finished_at TIMESTAMP AFTER time_lap");
//        DB::statement("ALTER TABLE race_results MODIFY COLUMN created_at TIMESTAMP  AFTER finished_at");
//        DB::statement("ALTER TABLE race_results MODIFY COLUMN updated_at TIMESTAMP  AFTER created_at");
//        DB::statement("ALTER TABLE statistics MODIFY COLUMN period INTEGER(11) AFTER wins");


        Schema::table('statistics', function (Blueprint $table) {
            $table->renameColumn('experience_change', 'experience');
            $table->renameColumn('gold_change', 'gold');
            $table->renameColumn('silver_change', 'silver');
        });
        Schema::table('statistics_cashed', function (Blueprint $table) {
            $table->renameColumn('experience_change', 'experience');
            $table->renameColumn('gold_change', 'gold');
            $table->renameColumn('silver_change', 'silver');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
