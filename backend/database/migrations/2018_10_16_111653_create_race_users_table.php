<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaceUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('race_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('race_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('ship_id');
            $table->integer('ship_state')->unsigned();
            $table->integer('score')->unsigned()->comment('race score at the finish');

            $table->timestamp('disconnected_at')->nullable();
            $table->timestamps();

            $table->foreign('race_id')->references('id')->on('races')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ship_id')->references('id')->on('user_ships')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("races_users");
    }
}
