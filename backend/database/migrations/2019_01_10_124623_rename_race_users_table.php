<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRaceUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('race_users', function (Blueprint $table) {
            $table->dropColumn('score');
            $table->integer('time_lap');
            $table->integer('experience');
            $table->integer('gold');
            $table->integer('silver');
            $table->integer('place');
        });

        Schema::rename('race_users', 'race_results');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
