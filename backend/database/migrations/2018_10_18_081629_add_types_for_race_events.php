<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesForRaceEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('race_events', function (Blueprint $table) {
            $table->renameColumn('room_id', 'race_id');
            $table->enum('type', ['send_word', 'received_word']);
            $table->string('word');
            $table->unsignedInteger('damage');

            $table->foreign('race_id')->references('id')->on('races')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
