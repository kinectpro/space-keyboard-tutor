<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_ships', function (Blueprint $table) {
            $table->dropForeign('user_ships_user_id_foreign');
        });

        Schema::rename('ships', 'ship_models');

        Schema::table('user_ships', function (Blueprint $table) {
            $table->renameColumn('ship_id', 'ship_model_id');

            $table->foreign('ship_model_id')->references('id')->on('ship_models')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
