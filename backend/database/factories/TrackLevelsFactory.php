<?php

use Faker\Generator as Faker;

$factory->define(\App\TrackLevels::class, function (Faker $faker) {
    return [
	    'track_level' => $faker->randomNumber(),
	    'min_level_word' => $faker->randomNumber(),
	    'max_level_word' => $faker->randomNumber()
    ];
});
