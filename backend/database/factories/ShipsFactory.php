<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\ShipModels::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'silver' => $faker->randomNumber(),
        'gold' => $faker->randomNumber(),
        'level' => $faker->numberBetween(0, 10),
        'fuel' => $faker->numberBetween(0, 100),
        'health' => $faker->numberBetween(0, 100),
    ];
});
