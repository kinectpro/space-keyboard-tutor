<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ShipModels;
use App\UserShips;

class UserShipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('user_ships')->delete();

        $this->createShip('championone@example.com', '0');
        $this->createShip('championone@example.com', '1');
        $this->createShip('championone@example.com', '2');
        $this->createShip('championone@example.com', '3');
        $this->createShip('championone@example.com', '4');
        $this->createShip('championone@example.com', '5');
        $this->createShip('championone@example.com', '6');
        $this->createShip('championone@example.com', '7');
        $this->createShip('championone@example.com', '8');
        $this->createShip('championone@example.com', '9');
        $this->createShip('championone@example.com', '10');


        $this->createShip('randomedude@example.com', '0');
        $this->createShip('randomedude@example.com', '1');
        $this->createShip('randomedude@example.com', '2');

        $this->createShip('happyhappy@example.com', '0');
        $this->createShip('happyhappy@example.com', '1');
        $this->createShip('happyhappy@example.com', '2');
        $this->createShip('happyhappy@example.com', '3');

        $this->createShip('loser@example.com', '0');
        $this->createShip('loser@example.com', '1');

        $this->createShip('yomama@example.com', '0');

    }

    function createShip($email, $level)
    {
        /** @var User $user */
        $user = User::where(User::FIELD_EMAIL, $email)->firstOrFail();
        $ship = ShipModels::where(ShipModels::FIELD_LEVEL, $level)->firstOrFail();

        $user->ships()->save(new UserShips([
            UserShips::FIELD_SHIP_MODEL_ID => $ship->id,
            UserShips::FIELD_HEALTH => $ship->health
        ]));
    }

}
