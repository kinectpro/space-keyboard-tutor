<?php

use Illuminate\Database\Seeder;
use App\Level;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('levels')->delete();

        $this->createLevel(1, 100, 3,3);
        $this->createLevel(2, 200, 3, 5);
        $this->createLevel(3, 300, 5, 7);
        $this->createLevel(4, 400, 7, 9);
        $this->createLevel(5, 500, 10,10);
        $this->createLevel(6, 600, 10,12);
        $this->createLevel(7, 700, 10,12);
        $this->createLevel(8, 800, 10,12);
        $this->createLevel(9, 900, 10,12);
    }

    /**
     * @param $level
     * @param $experience
     * @param $min
     * @param $max
     */
    function createLevel($level, $experience, $min, $max)
    {
        /** @var Level $newLevel */
        $newLevel = Level::firstOrNew([Level::FIELD_LEVEL => $level]);
        $newLevel->fill([
            Level::FIELD_LEVEL => $level,
            Level::FIELD_EXPERIENCE => $experience,
            Level::FIELD_MIN_LENGTH => $min,
            Level::FIELD_MAX_LENGTH => $max
        ]);
        $newLevel->save();
    }
}
