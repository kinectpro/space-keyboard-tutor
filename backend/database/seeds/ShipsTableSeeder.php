<?php

use Illuminate\Database\Seeder;
use App\ShipModels;

class ShipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('ship_models')->delete();

        $this->createShip('Sloop', null, null, 0, 10, 100);
        $this->createShip('Catboat', 5000, 100, 1, 10, 150);
        $this->createShip('Yacht', 10000, 200, 2, 12, 300);
        $this->createShip('Caravel', 20000, 400, 3, 12, 350);
        $this->createShip('Brig', 40000, 800, 4, 15, 500);
        $this->createShip('Brigantine', 80000, 1600, 5, 15, 750);
        $this->createShip('Corvette', 160000, 3000, 6, 18, 900);
        $this->createShip('Frigate', 320000, 6000, 7, 18, 1200);
        $this->createShip('Destroyer', 640000, 12000, 8, 20, 1500);
        $this->createShip('Cruiser', 1200000, 24000, 9, 20, 1800);
        $this->createShip('Dreadnought', 2500000, 50000, 10, 20, 2500);
    }

    function createShip($name, $silver, $gold, $level, $fuel, $health, $published = true)
    {
        factory(ShipModels::class)->create([
            ShipModels::FIELD_NAME => $name,
            ShipModels::FIELD_SILVER => $silver,
            ShipModels::FIELD_GOLD => $gold,
            ShipModels::FIELD_LEVEL => $level,
            ShipModels::FIELD_FUEL => $fuel,
            ShipModels::FIELD_HEALTH => $health,
            ShipModels::FIELD_IS_PUBLISHED => $published
        ]);
    }
}
