<?php

use Illuminate\Database\Seeder;
use App\StatisticsGlobal;
use App\User;
use App\UserShips;
use App\ShipModels;

class StatisticsCashedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('statistics_cashed')->delete();

        $this->createStatistics('championone@example.com','Sloop', 0, 0, 0, 0, 0, 0);
        $this->createStatistics('championone@example.com','Sloop', 0, 0, 0, 0, 0, 1);
        $this->createStatistics('championone@example.com','Sloop', 0, 0, 0, 0, 0, 7);
        $this->createStatistics('championone@example.com','Sloop', 0, 0, 0, 0, 0, 30);

        $this->createStatistics('randomedude@example.com','Sloop', 0, 0, 0, 0, 0, 0);
        $this->createStatistics('randomedude@example.com','Sloop', 0, 0, 0, 0, 0, 1);
        $this->createStatistics('randomedude@example.com','Sloop', 0, 0, 0, 0, 0, 7);
        $this->createStatistics('randomedude@example.com','Sloop', 0, 0, 0, 0, 0, 30);

        $this->createStatistics('happyhappy@example.com','Sloop', 0, 0, 0, 0, 0, 0);
        $this->createStatistics('happyhappy@example.com','Sloop', 0, 0, 0, 0, 0, 1);
        $this->createStatistics('happyhappy@example.com','Sloop', 0, 0, 0, 0, 0, 7);
        $this->createStatistics('happyhappy@example.com','Sloop', 0, 0, 0, 0, 0, 30);

        $this->createStatistics('loser@example.com','Sloop', 0, 0, 0, 0, 0, 0);
        $this->createStatistics('loser@example.com','Sloop', 0, 0, 0, 0, 0, 1);
        $this->createStatistics('loser@example.com','Sloop', 0, 0, 0, 0, 0, 7);
        $this->createStatistics('loser@example.com','Sloop', 0, 0, 0, 0, 0, 30);

        $this->createStatistics('yomama@example.com','Sloop', 0, 0, 0, 0, 0, 0);
        $this->createStatistics('yomama@example.com','Sloop', 0, 0, 0, 0, 0, 1);
        $this->createStatistics('yomama@example.com','Sloop', 0, 0, 0, 0, 0, 7);
        $this->createStatistics('yomama@example.com','Sloop', 0, 0, 0, 0, 0, 30);

    }

    /**
     * @param $email
     * @param $ship_name
     * @param $time_lap
     * @param $exp
     * @param $silver
     * @param $gold
     * @param $wins
     * @param $period
     */
    function createStatistics($email, $ship_name, $time_lap, $exp, $silver, $gold, $wins, $period)
    {
        /** @var StatisticsGlobal $newStatistics */
        $newStatistics = new StatisticsGlobal;
        $user = User::where(User::FIELD_EMAIL, $email)->firstOrFail();
        $ship = ShipModels::where(ShipModels::FIELD_NAME, $ship_name)->firstOrFail();
        $userShip = UserShips::where(UserShips::FIELD_USER_ID, $user->id)
            ->where(UserShips::FIELD_SHIP_MODEL_ID, $ship->id)->firstOrFail();
        $newStatistics->fill([
            StatisticsGlobal::FIELD_USER_ID => $user->id,
            StatisticsGlobal::FIELD_FAVORITE_SHIP_ID => $userShip->id,
            StatisticsGlobal::FIELD_TIME_LAP => $time_lap,
            StatisticsGlobal::FIELD_EXPERIENCE_CHANGE => $exp,
            StatisticsGlobal::FIELD_SILVER_CHANGE => $silver,
            StatisticsGlobal::FIELD_GOLD_CHANGE => $gold,
            StatisticsGlobal::FIELD_WINS => $wins,
            StatisticsGlobal::FIELD_PERIOD => $period
        ]);
        $newStatistics->save();
    }
}
