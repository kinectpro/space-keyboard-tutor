<?php

use Illuminate\Database\Seeder;

class TrackLevelsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	    \Illuminate\Support\Facades\DB::table('track_levels')->delete();

	    factory(App\TrackLevels::class)->create(['track_level' => 1, 'min_level_word' => 3, 'max_level_word' => 3]);
	    factory(App\TrackLevels::class)->create(['track_level' => 2, 'min_level_word' => 3, 'max_level_word' => 5]);
	    factory(App\TrackLevels::class)->create(['track_level' => 3, 'min_level_word' => 5, 'max_level_word' => 7]);
	    factory(App\TrackLevels::class)->create(['track_level' => 4, 'min_level_word' => 7, 'max_level_word' => 9]);
	    factory(App\TrackLevels::class)->create(['track_level' => 5, 'min_level_word' => 9, 'max_level_word' => 12]);
    }
}
