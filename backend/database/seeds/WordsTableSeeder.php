<?php

use Illuminate\Database\Seeder;
use App\Language;
use App\Word;
use Faker\Generator as Faker;

class WordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('words')->delete();

        for ($i = 1; $i <= 10; $i++) {
            $this->createWord('English', '3', '3');
            $this->createWord('English', '3', '5');
            $this->createWord('English', '5', '7');
            $this->createWord('English', '7', '9');
            $this->createWord('English', '10', '10');
            $this->createWord('English', '10', '12');
        }
    }


    /**
     * @param $language
     * @param $min
     * @param $max
     */
    function createWord($language, $min, $max)
    {
        /** @var Language $lang */
        $lang = Language::where(Language::FIELD_NAME, $language)->firstOrFail();
        $factory = new \Faker\Factory();
        $faker = $factory->create();
//        $word = $faker->realText($faker->numberBetween($min, $max));
        $word = $faker->lexify( str_repeat('?', $max));
        $lang->words()->save(new Word([
            Word::FIELD_WORD => $word
        ]));
    }
}
