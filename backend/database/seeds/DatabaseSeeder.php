<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(ShipsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserShipsTableSeeder::class);
	    $this->call(TrackLevelsTableSeeder::class);
        $this->call(LevelTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(WordsTableSeeder::class);
        $this->call(StatisticsCashedTableSeeder::class);
    }
}
