<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('admins')->delete();

        DB::table('admins')->insert([
            'email' => 'monax.at@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
