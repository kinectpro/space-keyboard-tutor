<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->delete();

        factory(App\User::class)->create(['email' => 'championone@example.com', 'password' => bcrypt('secret'), 'nickname' => 'ChampioneOne', 'experience' => '1548', 'dock_size' => '5', 'silver' => '99999', 'gold' => '1000', 'confirmed_at' => Now()]);
        factory(App\User::class)->create(['email' => 'randomedude@example.com', 'password' => bcrypt('secret'), 'nickname' => 'RandomeDude', 'experience' => '968', 'dock_size' => '5', 'silver' => '99999', 'gold' => '1000', 'confirmed_at' => Now()]);
        factory(App\User::class)->create(['email' => 'happyhappy@example.com', 'password' => bcrypt('secret'), 'nickname' => 'HappyHappy', 'experience' => '1001', 'dock_size' => '5', 'silver' => '99999', 'gold' => '1000', 'confirmed_at' => Now()]);
        factory(App\User::class)->create(['email' => 'loser@example.com', 'password' => bcrypt('secret'), 'nickname' => 'ImAmNOTALoser', 'experience' => '200', 'dock_size' => '5', 'silver' => '99999', 'gold' => '1000', 'confirmed_at' => Now()]);
        factory(App\User::class)->create(['email' => 'yomama@example.com', 'password' => bcrypt('secret'), 'nickname' => 'YoMama', 'experience' => '1245', 'dock_size' => '5', 'silver' => '99999', 'gold' => '1000', 'confirmed_at' => Now()]);

    }
}
