<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('languages')->delete();

       $this->createLanguage('English');
    }

    function createLanguage($name)
    {
        /** @var Language $newLanguage */
        $newLanguage = Language::firstOrNew([Language::FIELD_NAME => $name]);
        $newLanguage->fill([
            Language::FIELD_NAME => $name
        ]);
        $newLanguage->save();
    }
}
