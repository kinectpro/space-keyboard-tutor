<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    //return request()->route()->parameter('domain');
    return 'hi api';
});

Route::prefix('auth')->group(function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::post('confirm', 'Api\AuthController@confirm');

    Route::post('forgot-password', 'Api\AuthController@forgotPassword');
    Route::post('validate-password-token', 'Api\AuthController@validatePasswordToken');
    Route::post('reset-password', 'Api\AuthController@resetPassword');
});

Route::group(['prefix' => 'ships', 'middleware' => 'jwt.auth'], function () {
    Route::get('','Api\ShipsController@all');
//    Route::post('buy-silver/{ship_model_id}','Api\ShipsController@buyShipSilver');
//    Route::post('buy-gold/{ship_model_id}','Api\ShipsController@buyShipGold');
});

Route::group(['prefix' => 'users', 'middleware' => 'jwt.auth'], function () {
    Route::post('/profile/edit','Api\UsersController@edit');
    Route::post('/profile/change-password','Api\UsersController@changePassword');
    Route::get('', 'Api\UsersController@get');
});

Route::group(['prefix' => 'dock', 'middleware' => 'jwt.auth'], function () {
    Route::post('','Api\ShipsController@all');
    Route::put('/buy-ship-silver/{ship_model_id}','Api\ShipsController@buyShipSilver');
    Route::put('/buy-ship-gold/{ship_model_id}','Api\ShipsController@buyShipGold');
    Route::put('/to-repair/{ship_id}','Api\ShipsController@moveToRepair');
    Route::put('/from-repair/{ship_id}','Api\ShipsController@removeFromRepair');
    Route::put('/add-to-favorite/{ship_id}','Api\ShipsController@addToFavorite');
    Route::put('/remove-from-favorite/{ship_id}','Api\ShipsController@removeFromFavorite');
    Route::put('/get-health-status/{ship_id}','Api\ShipsController@getHealthStatus');
});

Route::group(['prefix' => 'profile', 'middleware' => 'jwt.auth'], function () {
Route::get('', 'Api\StatisticsController@get');
Route::get('statistic', 'Api\StatisticsController@statistic');
});

Route::group(['middleware' => 'jwt.refresh'], function(){
	Route::get('auth/refresh', 'Api\AuthController@refresh');
});