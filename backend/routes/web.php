<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\HomeController@index')
    ->middleware('auth:admin')
    ->name('dashboard');

// Authentication Routes...
Route::get('login', 'Admin\LoginController@showLoginForm')->name('login');
Route::post('login', 'Admin\LoginController@login');
Route::post('logout', 'Admin\LoginController@logout')->name('logout');

Route::prefix('users')->group(function () {
    Route::get('', 'Admin\UsersController@index')->name('users.index');

    Route::get('view/{id}', 'Admin\UsersController@view')->name('users.view');
    Route::get('edit/{id}', 'Admin\UsersController@edit')->name('users.edit');
    Route::post('update/{id}', 'Admin\UsersController@update')->name('users.update');

    Route::get('ban-or-unban/{id}', 'Admin\UsersController@banOrUnban')->name('users.ban-or-unban');
    Route::get('delete-or-restore/{id}', 'Admin\UsersController@deleteOrRestore')->name('users.delete-or-restore');
});

Route::prefix('ships')->group(function () {
    Route::get('index/{page?}/{sort?}/{filter?}', 'Admin\ShipsController@index')->name('ships.index');
    Route::get('view/{id}', 'Admin\ShipsController@view')->name('ships.view');

    Route::get('new', 'Admin\ShipsController@new')->name('ships.new');
    Route::get('edit/{id}', 'Admin\ShipsController@edit')->name('ships.edit');
    Route::post('save/{id?}', 'Admin\ShipsController@save')->name('ships.save');


    Route::get('delete/{id}', 'Admin\ShipsController@delete')->name('ships.delete');
    Route::get('publish/{id}', 'Admin\ShipsController@publish')->name('ships.publish');
    Route::get('enable/{id}', 'Admin\ShipsController@enable')->name('ships.enable');
});

Route::prefix('track-levels')->group(function () {
	Route::get('', 'Admin\TrackLevelsController@index')->name('track-levels.index');
	Route::get('/new', 'Admin\TrackLevelsController@new')->name('track-levels.new');
	Route::get('/edit/{id}', 'Admin\TrackLevelsController@edit')->name('track-levels.edit');
	Route::get('/delete/{id}', 'Admin\TrackLevelsController@delete')->name('track-levels.delete');
	Route::post('/save/{id?}', 'Admin\TrackLevelsController@save')->name('track-levels.save');
});

Route::prefix('lang')->group(function() {
    Route::get('', 'Admin\LanguagesController@index')->name('languages.index');
    Route::get('view/{id}', 'Admin\LanguagesController@view')->name('languages.view');
    Route::get('edit/{id}', 'Admin\LanguagesController@edit')->name('languages.edit');
    Route::get('delete/{id}', 'Admin\LanguagesController@delete')->name('languages.delete');
    Route::post('update/{id?}', 'Admin\LanguagesController@update')->name('languages.update');
    Route::get('new', 'Admin\LanguagesController@new')->name('languages.new');
    Route::get('from-file/{id?}', 'Admin\LanguagesController@fromFile')->name('languages.from-file');
    Route::post('upload', 'Admin\LanguagesController@upload')->name('languages.upload');
});

Route::prefix('dict')->group(function() {
    Route::get('', 'Admin\WordsController@index')->name('dictionary.index');
    Route::get('view/{id}', 'Admin\WordsController@view')->name('dictionary.view');
    Route::get('edit/{id}', 'Admin\WordsController@edit')->name('dictionary.edit');
    Route::get('delete/{id}', 'Admin\WordsController@delete')->name('dictionary.delete');
    Route::post('update/{id?}', 'Admin\WordsController@updateOrCreate')->name('dictionary.update');
    Route::get('new', 'Admin\WordsController@new')->name('dictionary.new');
});

Route::prefix('levels')->group(function () {
    Route::get('', 'Admin\LevelsController@index')->name('levels.index');
    Route::get('new', 'Admin\LevelsController@new')->name('levels.new');
    Route::get('view/{id}', 'Admin\LevelsController@view')->name('levels.view');
    Route::get('/edit/{id}', 'Admin\LevelsController@edit')->name('levels.edit');
    Route::get('/delete/{id}', 'Admin\LevelsController@delete')->name('levels.delete');
    Route::post('/save/{id?}', 'Admin\LevelsController@save')->name('levels.save');
});
