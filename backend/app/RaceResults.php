<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Events\UserCreated;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $ship_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property integer $experience
 * @property integer $silver
 * @property integer $gold
 * @property integer $place
 * @property integer $time_lap

 * @property UserShips $ship*
 * @property User $user*
 */
class RaceResults extends Model
{
    protected $table = 'race_results';

    use Notifiable, SoftDeletes, Sortable, FormAccessible;

    const FIELD_ID = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_SHIP_ID = 'ship_id';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_TIME_LAP = 'time_lap';
    const FIELD_EXPERIENCE_CHANGE = 'experience';
    const FIELD_SILVER_CHANGE = 'silver';
    const FIELD_GOLD_CHANGE = 'gold';
    const FIELD_PLACE = 'place';

    const RELATION_USER = 'user';
    const RELATION_SHIP = 'ship';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_SHIP_ID,
        self::FIELD_TIME_LAP,
        self::FIELD_EXPERIENCE_CHANGE,
        self::FIELD_SILVER_CHANGE,
        self::FIELD_GOLD_CHANGE,
        self::FIELD_PLACE
    ];

    public $sortable = [
        self::FIELD_ID,
        self::FIELD_USER_ID,
        self::FIELD_SHIP_ID,
        self::FIELD_CREATED_AT,
        self::FIELD_TIME_LAP,
        self::FIELD_EXPERIENCE_CHANGE,
        self::FIELD_SILVER_CHANGE,
        self::FIELD_GOLD_CHANGE,
        self::FIELD_PLACE
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, User::FIELD_ID, self::FIELD_USER_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ship()
    {
        return $this->hasOne(UserShips::class, User::FIELD_ID, self::FIELD_SHIP_ID);
    }
}
