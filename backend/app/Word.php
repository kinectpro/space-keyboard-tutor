<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use phpDocumentor\Reflection\Types\Self_;

/**
 * @property integer $language_id
 * @property string $word
*/
class Word extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'words';

    use Sortable;

    const FIELD_ID = 'id';
    const FIELD_LANGUAGE_ID = 'language_id';
    const FIELD_WORD = 'word';
    const FIELD_UPDATED_AT = 'updated_at';
    const FIELD_CREATED_AT = 'created_at';

    const RELATION_LANGUAGE = 'language';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_LANGUAGE_ID,
        self::FIELD_WORD
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public $sortable = [
        self::FIELD_ID,
        self::FIELD_WORD
    ];

    public function language()
    {
        return $this->belongsTo(Language::class)->withTrashed();
    }

}
