<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Collective\Html\Eloquent\FormAccessible;


/**
 * @property integer $id
 * @property string $name
 * @property integer $silver
 * @property integer $gold
 * @property integer $level
 * @property integer $fuel
 * @property integer $health
 * @property boolean $is_published
 * @property boolean $is_enabled
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property UserShips $userShip
 * @property UserShips[] $userShips
 */
class ShipModels extends Model
{
    use Sortable;
    use SoftDeletes;

    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_SILVER = 'silver';
    const FIELD_GOLD = 'gold';
    const FIELD_LEVEL = 'level';
    const FIELD_FUEL = 'fuel';
    const FIELD_HEALTH = 'health';
    const FIELD_IMAGE = 'image';
    const FIELD_IS_PUBLISHED ='is_published';
    const FIELD_IS_ENABLED = 'is_enabled';
    const FIELD_UPDATED_AT = 'updated_at';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_DELETED_AT = 'deleted_at';

    const RELATION_USER_SHIP = 'userShip';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_SILVER,
        self::FIELD_GOLD,
        self::FIELD_LEVEL,
        self::FIELD_FUEL,
        self::FIELD_HEALTH,
        self::FIELD_IMAGE,
        self::FIELD_UPDATED_AT
    ];
    public $sortable = [
        self::FIELD_LEVEL,
        self::FIELD_ID,
        self::FIELD_SILVER,
        self::FIELD_GOLD,
        self::FIELD_FUEL,
        self::FIELD_HEALTH,
        self::FIELD_NAME,
        self::FIELD_IS_PUBLISHED,
        self::FIELD_IS_ENABLED,
        self::FIELD_UPDATED_AT,
        self::FIELD_CREATED_AT
    ];

    public function buyShipSilver()
    {
        $userShip = new UserShips;
        $userShip->user_id = Auth::user()->id;
        $userShip->ship_model_id = $this->id;
        $userShip->health = $this->health;
        $userShip->save();

        $user = User::find(Auth::user()->id);
        $user->silver -= $this->silver;
        $user->save();
    }

    /**
     *
     */
    public function buyShipGold()
    {
        $userShip = new UserShips;
        $userShip->user_id = Auth::user()->id;
        $userShip->ship_model_id = $this->id;
        $userShip->health = $this->health;
        $userShip->save();

        $user = User::find(Auth::user()->id);
        $user->gold -= $this->gold;
        $user->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userShips() {
        return $this->hasMany(UserShips::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userShip()
    {
        return $this->hasOne(UserShips::class,UserShips::FIELD_SHIP_MODEL_ID)
            ->where(UserShips::FIELD_USER_ID, Auth::user()->id);
    }
}
