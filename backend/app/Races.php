<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Events\UserCreated;

/**
 * @property integer $id
 * @property integer $level
 * @property Carbon $created_at
 * @property Carbon $finished_at
 *
 * @property User $winner*
 */
class Races extends Model
{
    use Notifiable, SoftDeletes, Sortable, FormAccessible;

    const FIELD_ID = 'id';
    const FIELD_LEVEL = 'level';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_FINISHED_AT = 'finished_at';
    const FIELD_WINNER_ID = 'winner_user_id';

    const RELATION_WINNER_USER = 'winner';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_LEVEL,
        self::FIELD_WINNER_ID
    ];

    public $sortable = [
        self::FIELD_ID,
        self::FIELD_LEVEL,
        self::FIELD_CREATED_AT,
        self::FIELD_FINISHED_AT,
        self::FIELD_WINNER_ID
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function winner()
    {
        return $this->hasOne(User::class, User::FIELD_ID, self::FIELD_WINNER_ID);
    }

}
