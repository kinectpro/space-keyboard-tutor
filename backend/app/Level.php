<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use App\Events\UserCreated;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property integer $id
 * @property integer $level
 * @property integer $experience
 * @property integer $min_length
 * @property integer $max_length
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Level extends Model
{
    use Notifiable, Sortable;

    const FIELD_ID = 'id';
    const FIELD_LEVEL = 'level';
    const FIELD_EXPERIENCE = 'experience';
    const FIELD_MIN_LENGTH = 'min_length';
    const FIELD_MAX_LENGTH = 'max_length';
    const FIELD_UPDATED_AT = 'updated_at';
    const FIELD_CREATED_AT = 'created_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_LEVEL,
        self::FIELD_EXPERIENCE,
        self::FIELD_MIN_LENGTH,
        self::FIELD_MAX_LENGTH,
        self::FIELD_UPDATED_AT
    ];
    public $sortable = [
        self::FIELD_ID,
        self::FIELD_LEVEL,
        self::FIELD_EXPERIENCE,
        self::FIELD_MIN_LENGTH,
        self::FIELD_MAX_LENGTH,
        self::FIELD_UPDATED_AT,
        self::FIELD_CREATED_AT
    ];


}
