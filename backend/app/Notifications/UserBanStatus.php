<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserBanStatus extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $notifiable */
        /** @var String $line */
        if($notifiable->banned_at && $notifiable->unban_at)
        {
            $line = ('Your account have been baned at '. $notifiable->banned_at.'.\n'
                .'You will be unbaned at '. $notifiable->unban_at.'.\n');
        }
        if ($notifiable->banned_at && !$notifiable->unban_at)
        {
            $line = ('Duo to your behavior, your account have been baned at '. $notifiable->banned_at.
                ', and will not be restored until further notice.\n');
        }
        if(!$notifiable->banned_at)
        {
            $line = ('Your account have been unbaned at '. $notifiable->unban_at.'.\n'
                .'You can now return to the game !\n');
        }
        return (new MailMessage)
            ->greeting('Dear ' . $notifiable->nickname . '!')
            ->line($line)
            ->line('Thank you for playing Space Keyboard Tutor!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
