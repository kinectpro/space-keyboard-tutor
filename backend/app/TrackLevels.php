<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * @property integer $track_level
 * @property integer $min_level_word
 * @property integer $max_level_word
 *
 */
class TrackLevels extends Model
{
	use Sortable;
	
	const FIELD_ID = 'id';
	const FIELD_TRACK_LEVEL = 'track_level';
	const FIELD_MAX_LEVEL_WORD= 'max_level_word';
	const FIELD_MIN_LEVEL_WORD = 'min_level_word';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		self::FIELD_TRACK_LEVEL,
		self::FIELD_MIN_LEVEL_WORD,
		self::FIELD_MAX_LEVEL_WORD
	];
	
	public $sortable = [
		self::FIELD_TRACK_LEVEL,
		self::FIELD_MAX_LEVEL_WORD,
		self::FIELD_MIN_LEVEL_WORD
	];
}
