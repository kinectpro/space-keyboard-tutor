<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property Carbon $expired_at
 * @property Carbon created_at
 * @property Carbon $updated_at
 *
 * @property User $user
 */
class UserConfirmationTokens extends Model
{
    const FIELD_ID = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_TOKEN = 'token';
    const FIELD_EXPIRED_AT = 'expired_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_TOKEN,
        self::FIELD_EXPIRED_AT
    ];

    public function user()
    {
        return $this->belongsTo(User::class, self::FIELD_USER_ID);
    }
}
