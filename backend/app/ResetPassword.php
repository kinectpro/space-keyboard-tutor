<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ResetPassword extends Model
{
    use Notifiable;
    protected $table = 'reset_password';
    const FIELD_USER_ID = 'user_id';
    const FIELD_RESET_TOKEN = 'reset_token';
    const FIELD_EXPIRE_AT = 'expired_at';

    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_RESET_TOKEN,
        self::FIELD_EXPIRE_AT,
    ];

    protected $hidden = [
    ];
}
