<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\ShipModels;
use App\UserShips;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddStartingShip
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $ship = ShipModels::where(ShipModels::FIELD_LEVEL, 0)->firstOrFail();

        UserShips::create([
            UserShips::FIELD_SHIP_MODEL_ID => $ship->id,
            UserShips::FIELD_USER_ID => $event->user->id,
            UserShips::FIELD_HEALTH => $ship->health,
        ]);
    }
}
