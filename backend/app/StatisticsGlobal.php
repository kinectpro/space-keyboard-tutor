<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use App\Events\UserCreated;

/**
 * @property integer $id
 * @property integer $time_lap
 * @property integer $user_id
 * @property integer $user_ship_id
 * @property integer $experience_change
 * @property integer $silver_change
 * @property integer $gold_change
 * @property integer $wins
 * @property integer $period
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property UserShips $ship*
 * @property User $user*
 */
class StatisticsGlobal extends Model
{
    protected $table = 'statistics_cashed';

    use Notifiable, Sortable, FormAccessible;

    const FIELD_ID = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_FAVORITE_SHIP_ID = 'user_ship_id';
    const FIELD_TIME_LAP = 'time_lap';
    const FIELD_EXPERIENCE_CHANGE = 'experience';
    const FIELD_SILVER_CHANGE = 'silver';
    const FIELD_GOLD_CHANGE = 'gold';
    const FIELD_WINS = 'wins';
    const FIELD_PERIOD = 'period';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    const RELATION_USER = 'user';
    const RELATION_USER_SHIP = 'ship';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_FAVORITE_SHIP_ID,
        self::FIELD_TIME_LAP,
        self::FIELD_EXPERIENCE_CHANGE,
        self::FIELD_SILVER_CHANGE,
        self::FIELD_GOLD_CHANGE,
        self::FIELD_WINS,
        self::FIELD_PERIOD,
        self::FIELD_UPDATED_AT
    ];

    public $sortable = [
        self::FIELD_USER_ID,
        self::FIELD_FAVORITE_SHIP_ID,
        self::FIELD_TIME_LAP,
        self::FIELD_EXPERIENCE_CHANGE,
        self::FIELD_SILVER_CHANGE,
        self::FIELD_GOLD_CHANGE,
        self::FIELD_WINS,
        self::FIELD_PERIOD,
        self::FIELD_CREATED_AT,
        self::FIELD_UPDATED_AT
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, User::FIELD_ID, self::FIELD_USER_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ship()
    {
        return $this->hasOne(UserShips::class, User::FIELD_ID, self::FIELD_FAVORITE_SHIP_ID);
    }
}
