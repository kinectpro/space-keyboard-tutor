<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Language extends Model
{
    use Sortable;
    use SoftDeletes;

    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_UPDATED_AT = 'updated_at';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_DELETED_AT = 'deleted_at';

    protected $dates = [self::FIELD_DELETED_AT];

    const RELATION_WORD = 'word';
    const RELATION_WORDS = 'words';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_NAME
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public $sortable = [
        self::FIELD_ID,
        self::FIELD_NAME
    ];

    public function words()
    {
        return $this->hasMany(Word::class);
    }

    public function word()
    {
        return $this->hasOne(Word::class,Word::FIELD_LANGUAGE_ID);
    }

}
