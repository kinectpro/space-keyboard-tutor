<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

/**
 * @property integer $user_id
 * @property string $token
 * @property Carbon $expired_at
 * @property Carbon $created_at
 *
 * @property User $user
 */
class ResetPasswordTokens extends Model
{
    use Notifiable;

    const FIELD_USER_ID = 'user_id';
    const FIELD_TOKEN = 'token';
    const FIELD_EXPIRED_AT = 'expired_at';
    const FIELD_CREATED_AT = 'created_at';

    protected $fillable = [
        self::FIELD_USER_ID, self::FIELD_TOKEN, self::FIELD_EXPIRED_AT
    ];

    protected $hidden = [
    ];

    public function user() {
        return $this->belongsTo(User::class, self::FIELD_USER_ID);
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->user->email;
    }
}
