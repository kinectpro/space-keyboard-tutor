<?php

namespace App;

use App\Notifications\SignupActivate;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Events\UserCreated;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nickname
 * @property integer $dock_size
 * @property integer $silver
 * @property integer $gold
 * @property integer $experience
 * @property Carbon $confirmed_at
 * @property Carbon $banned_at
 * @property Carbon $unban_at
 * @property Carbon $deleted_at
 *
 * @property UserConfirmationTokens $lastConfirmationToken
 * @property UserShips[] $ships
 *
 * @property integer $level
 */
class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable, SoftDeletes, Sortable, FormAccessible;

    const FIELD_ID = 'id';
    const FIELD_EMAIL = 'email';
    const FIELD_PASSWORD = 'password';
    const FIELD_NICKNAME = 'nickname';
    const FIELD_DOCK_SIZE = 'dock_size';
    const FIELD_EXPERIENCE = 'experience';
    const FIELD_SILVER = 'silver';
    const FIELD_GOLD = 'gold';
    const FIELD_CONFIRMED_AT = 'confirmed_at';
    const FIELD_BANNED_AT = 'banned_at';
    const FIELD_UNBAN_AT = 'unban_at';
    const FIELD_DELETED_AT = 'deleted_at';

    const RELATION_USER_SHIPS = 'ships';

    protected $dates = [self::FIELD_DELETED_AT];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_NICKNAME,
        self::FIELD_EXPERIENCE,
        self::FIELD_DOCK_SIZE,
        self::FIELD_CONFIRMED_AT,
        self::FIELD_SILVER,
        self::FIELD_GOLD
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::FIELD_PASSWORD,
        'remember_token',
    ];

    public $sortable = [
        self::FIELD_EMAIL,
        self::FIELD_NICKNAME,
        self::FIELD_EXPERIENCE,
        self::FIELD_DOCK_SIZE,
        self::FIELD_CONFIRMED_AT,
        self::FIELD_BANNED_AT,
        self::FIELD_UNBAN_AT,
        self::FIELD_DELETED_AT
    ];


    public function sendConfirmationEmail()
    {
        $this->confirmationTokens()->save(new UserConfirmationTokens([
            UserConfirmationTokens::FIELD_EXPIRED_AT => Carbon::now()->addHour(),
            UserConfirmationTokens::FIELD_TOKEN => str_random(60)
        ]));
        $this->notify(new SignupActivate());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function confirmationTokens()
    {
        return $this->hasMany(UserConfirmationTokens::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastConfirmationToken()
    {
        return $this->hasOne(UserConfirmationTokens::class)
            ->orderBy(UserConfirmationTokens::FIELD_EXPIRED_AT, 'desc');
    }

    /**
    */
    public function ships() {
        return $this->hasMany(UserShips::class, UserShips::FIELD_USER_ID, self::FIELD_ID);
    }

    /**
     * @return boolean
    */
    public function hasFreeDock()
    {
        return $this->dock_size > $this->ships()
                ->whereNotNull(UserShips::FIELD_REPAIR_START_AT)
                ->count();
    }

    protected $dispatchesEvents = [
        'created' => UserCreated::class,
    ];


    public function userShips()
    {
        $this->hasMany(UserShips::class,UserShips::FIELD_USER_ID);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * @return int
     */
    public function level()
    {
        $levels = Level::all();
        $result = 0;
        for($i = 0; $i < $levels->count(); $i++)
        {
            if ($this->experience >= $levels[$i]->experience)
            {
                $result ++;
            }
        }
        return $result;
    }
}
