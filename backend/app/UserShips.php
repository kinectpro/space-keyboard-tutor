<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $ship_model_id
 * @property integer $health
 * @property Carbon $repair_start_at
 * @property Carbon repair_finish_at
 * @property Carbon favorite
 */
class UserShips extends Model
{
    const FIELD_ID = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_SHIP_MODEL_ID = 'ship_model_id';
    const FIELD_HEALTH = 'health';
    const FIELD_REPAIR_START_AT = 'repair_start_at';
    const FIELD_REPAIR_FINISH_AT = 'repair_finish_at';
    const RELATION_USER = 'user';
    const RELATION_SHIP_MODEL = 'shipModel';
    const FIELD_FAVORITE = 'favorite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_SHIP_MODEL_ID,
        self::FIELD_HEALTH,
        self::FIELD_REPAIR_START_AT,
        self::FIELD_REPAIR_FINISH_AT
    ];

    /**
     * @todo: need add logic for correct counting date of ending of repairing
     */
    public function moveToRepair()
    {
        $repair_end_date = Now();
        $this->repair_start_at = Now();
        $this->repair_finish_at = $repair_end_date;
        $this->save();
        return $this;
    }

    public function removeFromRepair()
    {
        $this->updateHealth();
        $this->repair_start_at = null;
        $this->repair_finish_at = null;
        $this->save();
        return $this;
    }

    public function addToFavorite()
    {
        $this->favorite = 1;
        $this->save();
        return $this;
    }

    public function removeFromFavorite()
    {
        $this->favorite = null;
        $this->save();
        return $this;
    }

    public function getHealthStatus()
    {
        $this->updateHealth();
        return $this;
    }

    private function updateHealth()
    {
        $health = $this->health + ((Carbon::parse($this->repair_start_at)->diffInSeconds(Now())) / 2);

        //die(var_dump($this->shipModel()->first()->health));
        $maxHealth = $this->shipModel()->first()->health;

        if($health < $maxHealth) {
            $this->health = $health;
        }
        else{
            $this->health = $maxHealth;
            $this->repair_start_at = null;
            $this->repair_finish_at = null;
            $this->save();
        }
    }

    public function user()
    {
        return $this->hasOne(User::class, User::FIELD_ID, self::FIELD_USER_ID);
    }

    public function shipModel()
    {
        return $this->hasOne(ShipModels::class, User::FIELD_ID, self::FIELD_SHIP_MODEL_ID);
    }
}
