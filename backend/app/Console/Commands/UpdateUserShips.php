<?php

namespace App\Console\Commands;

use App\UserShips;
use Illuminate\Console\Command;

class UpdateUserShips extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:userShips';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update health user ship';

    /**
     * The user ship.
     *
     * @var UserShips
     */
    protected $userShips;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        //$this->userShips = $userShips;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userShips = UserShips::all();

        foreach($userShips as $ship) {
            $ship->updateHealth();
        }
    }
}
