<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\UserBanStatus;

class UserBanedState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:checkBanStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check baned users status and unbans when necessary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where(User::FIELD_BANNED_AT ?? User::FIELD_UNBAN_AT)->get();
        foreach ($users as $user)
        {
                if (Carbon::now()->greaterThanOrEqualTo(Carbon::parse($user->unban_at)))
                {
                    $user->banned_at = null;
                    $user->unban_at = null;
                    $user->notify(new UserBanStatus());
                    echo "User " .$user->nickname. "has been unbaned"."\n";
                }
        }
    }
}
