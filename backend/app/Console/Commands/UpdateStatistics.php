<?php

namespace App\Console\Commands;

use App\RaceResults;
use App\StatisticsGlobal;
use App\StatisticsSingle;
use App\User;
use App\ShipModels;
use App\UserShips;
use Illuminate\Console\Command;
use Carbon\Carbon;

class UpdateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:usersStatistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Users Statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */

    public function handle()
    {
        /**
         * @param $arr
         * @param $n
         * @return mixed
         */
        function mostFrequent($arr, $n)
        {
            sort($arr);
            sort($arr, $n);
            $max_count = 1;
            $res = $arr[0];
            $curr_count = 1;
            for ($i = 1; $i < $n; $i++) {
                if ($arr[$i] == $arr[$i - 1]) {
                    $curr_count++;
                } else {
                    if ($curr_count > $max_count) {
                        $max_count = $curr_count;
                        $res = $arr[$i - 1];
                    }
                    $curr_count = 1;
                }
            }
            return $res;
        }

        /**
         * @var StatisticsSingle[] $statisticsDaily
         * @var StatisticsGlobal[] $statisticsCashed
         * @var RaceResults[] $raceResults
         */
        $dateToday = Carbon::today()->subDay();
        $dateMonth = $dateToday->subDays(29);
        $users = User::all();
        $days = StatisticsSingle::all();
        $statisticsDaily = StatisticsSingle::where('created_at', '>', $dateMonth->toDateTimeString())->orderBy(StatisticsSingle::FIELD_CREATED_AT, 'desc')->get();
        $statisticsCashed = StatisticsGlobal::all();
        $raceResults = RaceResults::where('created_at', '=', $dateToday->toDateTimeString())->get();

        foreach ($users as $user)
        {

            $newDay = [];
            $newMonth = [];
            $newWeek = [];
            $newTotal = [];

            /**
             * @var StatisticsSingle[] $lastSix
             * @var StatisticsSingle[] $lastMonth
             * @var StatisticsGlobal $week
             * @var StatisticsGlobal $month
             * @var StatisticsGlobal $total
             */
            $lastSix = $statisticsDaily->where(StatisticsSingle::FIELD_USER_ID, $user::FIELD_ID)
                ->where('created_at', '>', $dateToday->subDays(6)->toDateTimeString());
            $lastMonth = $statisticsDaily->where(StatisticsSingle::FIELD_USER_ID, $user::FIELD_ID);
            $week = $statisticsCashed->where(StatisticsGlobal::FIELD_PERIOD, 7)
                ->where(StatisticsGlobal::FIELD_USER_ID, $user::FIELD_ID);
            $month = $statisticsCashed->where(StatisticsGlobal::FIELD_PERIOD, 30)
                ->where(StatisticsGlobal::FIELD_USER_ID, $user::FIELD_ID);
            $total = $statisticsCashed->where(StatisticsGlobal::FIELD_PERIOD, null)
                ->where(StatisticsGlobal::FIELD_USER_ID, $user::FIELD_ID);

            // New Day to Statistics
            $racesCount = 0;
            $totalTime = 0;
            $shipsIDs = [];
            foreach ($raceResults as $raceResult) {
                if ($raceResult::FIELD_USER_ID == $user::FIELD_ID) {
                    if ($raceResult::FIELD_PLACE == 1) {
                        $newDay[StatisticsSingle::FIELD_WINS]++;
                    }
                    $newDay[StatisticsSingle::FIELD_SILVER_CHANGE] += $raceResult::FIELD_SILVER_CHANGE;
                    $newDay[StatisticsSingle::FIELD_GOLD_CHANGE] += $raceResult::FIELD_GOLD_CHANGE;
                    $newDay[StatisticsSingle::FIELD_EXPERIENCE_CHANGE] += $raceResult::FIELD_EXPERIENCE_CHANGE;
                    $totalTime += $raceResult::FIELD_TIME_LAP;
                    $racesCount ++;
                    array_push($shipsIDs, $raceResult::FIELD_USER_SHIP_ID);
                }
            }
            $newDay[StatisticsSingle::FIELD_USER_ID] = $user::FIELD_ID;
            $newDay[StatisticsSingle::FIELD_TIME_LAP] = $totalTime/$racesCount;
            $n = sizeof($shipsIDs) / sizeof($shipsIDs[0]);
            $newDay[StatisticsSingle::FIELD_USER_SHIP_ID] = mostFrequent($shipsIDs, $n);
            $newDay[StatisticsSingle::FIELD_PERIOD] = 1;
            StatisticsSingle::create($newDay);
            unset($shipsIDs);

            // New Week to Statistics
            $racesCount = 0;
            $totalTime = 0;
            $shipsIDs = [];
            $newWeek[StatisticsGlobal::FIELD_WINS] += $newDay[StatisticsSingle::FIELD_WINS];
            $newWeek[StatisticsGlobal::FIELD_SILVER_CHANGE] += $newDay[StatisticsSingle::FIELD_SILVER_CHANGE];
            $newWeek[StatisticsGlobal::FIELD_GOLD_CHANGE] += $newDay[StatisticsSingle::FIELD_GOLD_CHANGE];
            $newWeek[StatisticsGlobal::FIELD_EXPERIENCE_CHANGE] += $newDay[StatisticsSingle::FIELD_EXPERIENCE_CHANGE];
            $totalTime += $newDay[StatisticsSingle::FIELD_TIME_LAP];
            $racesCount ++;
            array_push($shipsIDs, $newDay[StatisticsSingle::FIELD_USER_SHIP_ID]);

            foreach($lastSix as $day){
                $newWeek[StatisticsGlobal::FIELD_WINS] += $day::FIELD_WINS;
                $newWeek[StatisticsGlobal::FIELD_SILVER_CHANGE] += $day::FIELD_SILVER_CHANGE;
                $newWeek[StatisticsGlobal::FIELD_GOLD_CHANGE] += $day::FIELD_GOLD_CHANGE;
                $newWeek[StatisticsGlobal::FIELD_EXPERIENCE_CHANGE] += $day::FIELD_EXPERIENCE_CHANGE;
                $totalTime += $day::FIELD_TIME_LAP;
                $racesCount ++;
                array_push($shipsIDs, $day::FIELD_USER_SHIP_ID);
            }
            $newWeek[StatisticsGlobal::FIELD_ID] = $week::FIELD_ID;
            $newWeek[StatisticsGlobal::FIELD_USER_ID] = $user::FIELD_ID;
            $newWeek[StatisticsGlobal::FIELD_TIME_LAP] = $totalTime/$racesCount;
            $n = sizeof($shipsIDs) / sizeof($shipsIDs[0]);
            $newWeek[StatisticsGlobal::FIELD_FAVORITE_SHIP_ID] = mostFrequent($shipsIDs, $n);
            $newWeek[StatisticsGlobal::FIELD_PERIOD] = 7;
            $week->fill($newWeek);
            $week->save();
            unset($shipsIDs);

            // New Month to Statistics
            $racesCount = 0;
            $totalTime = 0;
            $shipsIDs = [];
            $newMonth[StatisticsGlobal::FIELD_WINS] += $newDay[StatisticsSingle::FIELD_WINS];
            $newMonth[StatisticsGlobal::FIELD_SILVER_CHANGE] += $newDay[StatisticsSingle::FIELD_SILVER_CHANGE];
            $newMonth[StatisticsGlobal::FIELD_GOLD_CHANGE] += $newDay[StatisticsSingle::FIELD_GOLD_CHANGE];
            $newMonth[StatisticsGlobal::FIELD_EXPERIENCE_CHANGE] += $newDay[StatisticsSingle::FIELD_EXPERIENCE_CHANGE];
            $totalTime += $newDay[StatisticsSingle::FIELD_TIME_LAP];
            $racesCount ++;
            array_push($shipsIDs, $newDay[StatisticsSingle::FIELD_USER_SHIP_ID]);

            foreach($lastMonth as $day){
                $newMonth[StatisticsGlobal::FIELD_WINS] += $day::FIELD_WINS;
                $newMonth[StatisticsGlobal::FIELD_SILVER_CHANGE] += $day::FIELD_SILVER_CHANGE;
                $newMonth[StatisticsGlobal::FIELD_GOLD_CHANGE] += $day::FIELD_GOLD_CHANGE;
                $newMonth[StatisticsGlobal::FIELD_EXPERIENCE_CHANGE] += $day::FIELD_EXPERIENCE_CHANGE;
                $totalTime += $day::FIELD_TIME_LAP;
                $racesCount ++;
                array_push($shipsIDs, $day::FIELD_USER_SHIP_ID);
            }
            $newMonth[StatisticsGlobal::FIELD_ID] = $month::FIELD_ID;
            $newMonth[StatisticsGlobal::FIELD_USER_ID] = $user::FIELD_ID;
            $newMonth[StatisticsGlobal::FIELD_TIME_LAP] = $totalTime/$racesCount;
            $n = sizeof($shipsIDs) / sizeof($shipsIDs[0]);
            $newMonth[StatisticsGlobal::FIELD_FAVORITE_SHIP_ID] = mostFrequent($shipsIDs, $n);
            $newMonth[StatisticsGlobal::FIELD_PERIOD] = 30;
            $month->fill($newMonth);
            $month->save();
            unset($shipsIDs);

            // New Total to Statistics
            $racesCount = 0;
            $totalTime = 0;
            $shipsIDs = [];
            $newTotal[StatisticsGlobal::FIELD_WINS] = $newDay[StatisticsSingle::FIELD_WINS] + $total::FIELD_WINS;
            $newTotal[StatisticsGlobal::FIELD_SILVER_CHANGE] = $newDay[StatisticsSingle::FIELD_SILVER_CHANGE] + $total::FIELD_SILVER_CHANGE;
            $newTotal[StatisticsGlobal::FIELD_GOLD_CHANGE] = $newDay[StatisticsSingle::FIELD_GOLD_CHANGE] + $total::FIELD_GOLD_CHANGE;
            $newTotal[StatisticsGlobal::FIELD_EXPERIENCE_CHANGE] = $newDay[StatisticsSingle::FIELD_EXPERIENCE_CHANGE] + $total::FIELD_EXPERIENCE_CHANGE;
            $totalTime = $newDay[StatisticsSingle::FIELD_TIME_LAP];
            $racesCount ++;
            array_push($shipsIDs, $newDay[StatisticsSingle::FIELD_USER_SHIP_ID]);

            foreach($days as $day){
                $totalTime += $day::FIELD_TIME_LAP;
                $racesCount ++;
                array_push($shipsIDs, $day::FIELD_USER_SHIP_ID);
            }
            $newTotal[StatisticsGlobal::FIELD_ID] = $total::FIELD_ID;
            $newTotal[StatisticsGlobal::FIELD_USER_ID] = $user::FIELD_ID;
            $newTotal[StatisticsGlobal::FIELD_TIME_LAP] = $totalTime/$racesCount;
            $n = sizeof($shipsIDs) / sizeof($shipsIDs[0]);
            $newTotal[StatisticsGlobal::FIELD_FAVORITE_SHIP_ID] = mostFrequent($shipsIDs, $n);
            $newTotal[StatisticsGlobal::FIELD_PERIOD] = 0;
            $total->fill($newTotal);
            $total->save();
            unset($shipsIDs);
        }

    }
}
