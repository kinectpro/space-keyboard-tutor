<?php
/**
 * Created by PhpStorm.
 * User: Alex Wade
 * Date: 24.10.2018
 * Time: 10:36
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ShipModels;
use App\UserShips;
use App\Http\Controllers\Admin\ImagesController;
use Illuminate\Support\Facades\Auth;
use Validator;

/**
 * Class ShipsController
 * @package App\Http\Controllers\API
 */
class ShipsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json((ShipModels::where(ShipModels::FIELD_IS_PUBLISHED, true)
            ->with(ShipModels::RELATION_USER_SHIP)->get()), 200);
    }

    /**
     * @param $ship_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function moveToRepair($ship_id)
    {
        /** @var UserShips $ship */
        if (!($ship = Auth::user()->ships()->where(UserShips::FIELD_ID, $ship_id)->first())) {
            return response()->json([UserShips::FIELD_ID => 'Wrong ship id'], 404);
        }

        if (($ship->repair_start_at)) {
            return response()->json([UserShips::FIELD_USER_ID => 'Already repairing'], 400);
        }

        if (!Auth::user()->hasFreeDock()) {
            return response()->json(['error' => 'No free docks'], 400);
        }

        $ship->moveToRepair();
        return response()->json(['status' => 'success','ship' => $ship]);
    }

    /**
     * @param integer $ship_id
     * @return \Illuminate\Http\JsonResponse
    */
    public function removeFromRepair($ship_id)
    {
        /** @var UserShips $ship */
        if (!($ship = Auth::user()->ships()->where(UserShips::FIELD_ID, $ship_id)->first())) {
            return response()->json([UserShips::FIELD_ID => 'Wrong ship id'], 404);
        }
        $ship->removeFromRepair();

        return response()->json(['status' => 'success','ship' => $ship]);
    }

    /**
     * @param integer $ship_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToFavorite($ship_id)
    {
        /** @var UserShips $ship */
        if (!($ship = Auth::user()->ships()->where(UserShips::FIELD_ID, $ship_id)->first())) {
            return response()->json([UserShips::FIELD_ID => 'Wrong ship id'], 404);
        }

        $ship->addToFavorite();

        return response()->json(['status' => 'success','ship' => $ship]);
    }

    /**
     * @param integer $ship_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromFavorite($ship_id)
    {
        /** @var UserShips $ship */
        if (!($ship = Auth::user()->ships()->where(UserShips::FIELD_ID, $ship_id)->first())) {
            return response()->json([UserShips::FIELD_ID => 'Wrong ship id'], 404);
        }

        $ship->removeFromFavorite();

        return response()->json(['status' => 'success','ship' => $ship]);
    }

    /**
     * @param integer $ship_id
     * @return \Illuminate\Http\JsonResponse
    */
    public function getHealthStatus($ship_id)
    {
        /** @var UserShips $ship */
        if (!($ship = Auth::user()->ships()->where(UserShips::FIELD_ID, $ship_id)->first())) {
            return response()->json([UserShips::FIELD_ID => 'Wrong ship id'], 404);
        }
        $ship->getHealthStatus();

        return response()->json(['ship' => $ship]);
    }

    /**
     * @param $ship_model_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function buyShipSilver($ship_model_id)
    {
        /** @var ShipModels $ship */
        if (!($ship = ShipModels::find($ship_model_id))) {
            return response()->json(['error' => 'Ship model not found'], 404);
        }

        /** @var User $user */
        $user = Auth::user();
        if ($user->ships()->where(UserShips::FIELD_SHIP_MODEL_ID, $ship_model_id)->first()) {
            return response()->json(['error' => 'Ship already owned'], 400);
        }

        if ($user->silver < $ship->silver) {
            return response()->json(['error' => 'Not enough silver'], 400);
        }
        //@todo $user->level
        if ($user->level() < $ship->level) {
            return response()->json(['error' => 'Your level is too low for buying this ship'], 400);
        }

        $ship->buyShipSilver();

        return response()->json(['status' => 'success']);
    }

    /**
     * @param $ship_model_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function buyShipGold($ship_model_id)
    {
        /** @var ShipModels $ship */
        if (!($ship = ShipModels::find($ship_model_id))) {
            return response()->json(['error' => 'Ship model not found'], 404);
        }

        /** @var User $user */
        $user = Auth::user();
        if ($user->ships()->where(UserShips::FIELD_SHIP_MODEL_ID, $ship_model_id)->first()) {
            return response()->json(['error' => 'Ship already owned'], 400);
        }

        if ($user->gold < $ship->gold) {
            return response()->json(['error' => 'Not enough gold'], 400);
        }
        //@todo $user->level
        if ($user->getLevel() < $ship->level) {
            return response()->json(['error' => 'Your level is too low for buying this ship'], 400);
        }

        $ship->buyShipGold();

        return response()->json(['status' => 'success']);
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Http\JsonResponse|null
     */
    public function getUser()
    {
        return response()->json(Auth::user(), 200);
    }
}
