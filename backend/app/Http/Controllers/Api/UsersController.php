<?php

namespace App\Http\Controllers\API;


use App\UserConfirmationTokens;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ResetPasswordTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Notifications\ForgotPassword;
use Tymon\JWTAuth\Facades\JWTAuth;


/**
 * Class UsersController
 * @package App\Http\Controllers\API
 */
class UsersController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get() {
        return response()->json(Auth::user());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request)
    {
        /** @var User $user */
        $this->validate($request, [
            User::FIELD_NICKNAME => 'required|unique:users',
//            'gender' => 'required',
//            'age' => 'required|integer',
        ]);
        $user = Auth::user();
        $user::FIELD_NICKNAME = request(User::FIELD_NICKNAME);
//        $user::FIELD_GENDER = request(User::FIELD_GENDER);
//        $user::FIELD_AGE = request(User::FIELD_AGE);
        $user->save();
        return response()->json(['status' => 'success']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changePassword(Request $request)
    {
        /** @var User $user */
        $this->validate($request, [
            'oldPassword' => 'required',
            'password' => 'required|min:4',
            'confirmPassword' => 'required|min:4'
        ]);
        $user = Auth::user();
        if ($user::FIELD_PASSWORD != bcrypt($request->oldPassword))
        {
            return response()->json([User::FIELD_PASSWORD => "Enter correct old password !"], 400);
        }
        if ($request->password != $request->confirmPassword)
        {
            return response()->json(['error' => 'New Password does not match up with its confirmation !'], 400);
        }
        $user::FIELD_PASSWORD = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success']);
    }
}