<?php

namespace App\Http\Controllers\API;

use App\Mail\ConfirmEmail;
use App\UserConfirmationTokens;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ResetPasswordTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Notifications\SignupActivate;
use App\Notifications\ForgotPassword;
use Psr\Log\NullLogger;


class ProfileController extends Controller
{
    public function get()
    {
        return response()->json(Auth::user());
    }

    public function statistic()
    {
        $statistic = [
            'day' => [
                [
                    'id' => 3,
                    'nickname' => 'Nickname1',
                    'level' => 11,
                    'time' => 45,
                    'favorite' => 'Droid',
                    'gold' => 122,
                    'silver' => 1250,
                    'wins' => 54,
                    'exp' => 12568
                ], [
                    'id' => 2,
                    'nickname' => 'Nickname2',
                    'level' => 11,
                    'time' => 45,
                    'favorite' => 'Droid',
                    'gold' => 122,
                    'silver' => 1250,
                    'wins' => 54,
                    'exp' => 12568
                ], [
                    'id' => 1,
                    'nickname' => 'Nickname3',
                    'level' => 11,
                    'time' => 45,
                    'favorite' => 'Droid',
                    'gold' => 122,
                    'silver' => 1250,
                    'wins' => 54,
                    'exp' => 12568
                ],
            ],
            'week' => [],
            'month' => [],
            'all' => []
        ];
        return response()->json($statistic, 200, [], JSON_FORCE_OBJECT);
    }
}

