<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 19.10.18
 * Time: 15:51
 */

namespace App\Http\Controllers\API;

use App\UserConfirmationTokens;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ResetPasswordTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Notifications\ForgotPassword;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            /** @var User $user */
            $user = User::where(User::FIELD_EMAIL, request(User::FIELD_EMAIL))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json([User::FIELD_EMAIL => "Can't find your profile"], 404);
        }

	    if (!$user->confirmed_at) {
            $user->sendConfirmationEmail();
		    return response()->json([User::FIELD_EMAIL => "Your profile not confirmed. We sent profile confirmation email for you."], 403);
        }
        if ($user->banned_at) {
            return response()->json([User::FIELD_EMAIL => "Your profile is baned"], 403);
        }

        if ($token = Auth::guard('api')->attempt([
            User::FIELD_EMAIL => request(User::FIELD_EMAIL),
            User::FIELD_PASSWORD => request(User::FIELD_PASSWORD)
        ])) {
            return $this->respondWithToken($token);
        }

        return response()->json([User::FIELD_EMAIL => 'Wrong email or password'], 401);
//         return response()->json([User::FIELD_EMAIL => 'Wrong '], 401);

    }

    /**
     * Register api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            User::FIELD_EMAIL => 'required|email',
            User::FIELD_PASSWORD => 'required|min:8'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 401);
        }
        if (User::where(User::FIELD_EMAIL, request(User::FIELD_EMAIL))->exists()) {
            return response()->json([User::FIELD_EMAIL => "User already exists"], 403);
        }

        /** @var User $user */
        $user = User::create([
            User::FIELD_EMAIL => request(User::FIELD_EMAIL),
            User::FIELD_NICKNAME => explode('@', request(User::FIELD_EMAIL))[0],
            User::FIELD_PASSWORD => bcrypt(request(User::FIELD_PASSWORD)),
            User::FIELD_EXPERIENCE => 0,
            User::FIELD_DOCK_SIZE => 3,
        ]);

        $user->createConfirmationToken();

        return response()->json(['message' => 'Successfully created user'], 201);
    }

    /**
     * Confirm account (activate user)
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm()
    {
        /** @var UserConfirmationTokens $token */
        $token = UserConfirmationTokens::where(UserConfirmationTokens::FIELD_TOKEN, request(UserConfirmationTokens::FIELD_TOKEN))
            ->where(UserConfirmationTokens::FIELD_EXPIRED_AT, '>', Carbon::now())
            ->first();

        if (!$token) {
            return response()->json([
                UserConfirmationTokens::FIELD_TOKEN => "Can't find your confirmation token or your token is expired"
            ], 404);
        }

        $token->user->confirmed_at = Carbon::now();
        $token->user->save();
        $token->user->confirmationTokens()->delete();

        return response()->json(['status' => 'success'], 200);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            User::FIELD_EMAIL => 'required|email'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {
            /** @var User $user */
            $user = User::where(User::FIELD_EMAIL, request(User::FIELD_EMAIL))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json([User::FIELD_EMAIL => "Can't find your profile"], 404);
        }

        /** @var ResetPasswordTokens $reset */
        $reset = ResetPasswordTokens::create([
            ResetPasswordTokens::FIELD_USER_ID => $user->id,
            ResetPasswordTokens::FIELD_TOKEN => str_random(60),
            ResetPasswordTokens::FIELD_EXPIRED_AT => Carbon::now()->addHours(2)
        ]);

        $reset->notify(new ForgotPassword());

        return response()->json(['message' => 'Successfully send email'], 201);
    }

    public function validatePasswordToken(Request $request) {
        /** @var ResetPasswordTokens $token */
        if (!ResetPasswordTokens::where(ResetPasswordTokens::FIELD_TOKEN, $request->input('token'))
            ->where(ResetPasswordTokens::FIELD_EXPIRED_AT, '>', now())
            ->exists()) {
            return response()->json([ResetPasswordTokens::FIELD_TOKEN => "Token can't find or expired"], 404);
        }

        return response()->json([ResetPasswordTokens::FIELD_TOKEN => "Token has been validated"]);
    }

    public function resetPassword(Request $request)
    {
        try {
            /** @var ResetPasswordTokens $token */
            $token = ResetPasswordTokens::where(ResetPasswordTokens::FIELD_TOKEN, $request->input('token'))
                ->where(ResetPasswordTokens::FIELD_EXPIRED_AT, '>', now())
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json("Token can't find or expired", 404);
        }

        $validator = Validator::make($request->all(), [
            User::FIELD_PASSWORD => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $token->user->password = bcrypt($request->input('password'));
        $token->user->save();

        return response()->json(['message' => 'Password updated'], 201);
    }
	
	public function refresh()
	{
		return response([
			'status' => 'success'
		]);
	}


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
        ]);
    }
}
