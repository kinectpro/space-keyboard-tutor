<?php
namespace App\Http\Controllers\API;

use App\ShipModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserShips;
use App\StatisticsSingle;
use App\StatisticsGlobal;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Mail\ConfirmEmail;
use App\UserConfirmationTokens;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\ResetPasswordTokens;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Notifications\SignupActivate;
use App\Notifications\ForgotPassword;
use Psr\Log\NullLogger;

/**
 * Class StatisticsController
 * @package App\Http\Controllers\API
 */
class StatisticsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return response()->json(Auth::user());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function statistic()
    {
        /**
         * @param $arr
         * @param $n
         * @return mixed
         */
        function mostFrequent($arr, $n)
        {
            sort($arr);
            sort($arr, $n);
            $max_count = 1;
            $res = $arr[0];
            $curr_count = 1;
            for ($i = 1; $i < $n; $i++) {
                if ($arr[$i] == $arr[$i - 1]) {
                    $curr_count++;
                } else {
                    if ($curr_count > $max_count) {
                        $max_count = $curr_count;
                        $res = $arr[$i - 1];
                    }
                    $curr_count = 1;
                }
            }
            return $res;
        }

        /**
         * @var ShipModels[] $ships
         */
        $dailyStat = StatisticsSingle::whereDate('created_at', Carbon::today())->get();
//        $globalStat = StatisticsGlobal::where('period', '!=' , 1)->get();
        $globalStat = StatisticsGlobal::all();
        $users = User::all();
        $ships = UserShips::all();
        $statistic = [
            'day' => [],
            'week' => [],
            'month' => [],
            'all' => []
        ];

        foreach ($users as $user)
        {
            /**
             * @var StatisticsSingle[] $userDailys
             * @var StatisticsGlobal[] $userGlobals
             */
//            $userDailys = $dailyStat->where(StatisticsSingle::FIELD_USER_ID, $user::FIELD_ID);
            $userGlobals = $globalStat->where(StatisticsGlobal::FIELD_USER_ID, $user->id);
//            $userStatDaily = [];
            $userStatGeneral = [];
            $shipsIDs = [];

            // Collecting user daily statistics
//            $userStatDaily['id'] = $user::FIELD_ID;
//            $userStatDaily['nickname'] = $user::FIELD_NICKNAME;
//            $userStatDaily['level'] = $user->level();
//            foreach ($userDailys as $d)
//            {
//                $userStatDaily['time'] += $d::FIELD_TIME_LAP;
//                $userStatDaily['exp'] += $d::FIELD_EXPERIENCE_CHANGE;
//                $userStatDaily['gold'] += $d::FIELD_GOLD_CHANGE;
//                $userStatDaily['silver'] += $d::FIELD_SILVER_CHANGE;
//                $userStatDaily['wins'] += $d::FIELD_WINS;
//                array_push($shipsIDs, $d::FIELD_USER_SHIP_ID);
//            }
//            $n = sizeof($shipsIDs) / sizeof($shipsIDs[0]);
//            $mostFrqId = mostFrequent($shipsIDs, $n);
//            $userStatDaily['time'] = $userStatDaily['time']/count($userStatDaily);
//            foreach ($ships as $ship)
//            {
//                if ($ship::FIELD_ID == $mostFrqId)
//                {
//                    $userStatDaily['favorite'] = $ship->shipModel()->name;
//                }
//            }
//            usort($userStatDaily, function($a, $b) {return $a['exp'] <=> $b['exp'];});
//            array_push($day, $userStatDaily);
//            array_push($statistic, $day);

            // Collecting user global statistics
            foreach ($userGlobals as $glob)
            {
                $faw = '';
                foreach ($ships as $ship)
                {
                    if ($ship->id == $glob->user_ship_id)
                    {
                        $faw = $ship->shipModel->name;
                    }
                }

                $userStatGeneral = [
                    'id' => $user->id,
                    'nickname' => $user->nickname,
                    'level' => $user->level(),
                    'time' => $glob->time_lap,
                    'favorite' => $faw,
                    'gold' => $glob->gold,
                    'silver' => $glob->silver,
                    'wins' => $glob->wins,
                    'exp' => $glob->experience
                ];

                switch ($glob->period) {
                    case '1':
                        array_push($statistic['day'], $userStatGeneral);
                        break;
                    case '7':
                        array_push($statistic['week'], $userStatGeneral);
                        break;
                    case '30':
                        array_push($statistic['month'], $userStatGeneral);
                        break;
                    case '0':
                        array_push($statistic['all'], $userStatGeneral);
                        break;
                }
            }
        }
        return response()->json($statistic, 200, [], JSON_FORCE_OBJECT);
    }
}