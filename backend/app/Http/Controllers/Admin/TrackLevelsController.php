<?php

namespace App\Http\Controllers\Admin;

use App\TrackLevels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TrackLevelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('track-levels.index', [
            'trackLevels' => TrackLevels::sortable()->paginate(10)
        ]);
    }

    public function new()
    {
        return view('track-levels.new');
    }

    public function edit($id)
    {
        return view('track-levels.edit', [
            'trackLevel' => TrackLevels::findOrFail($id)
        ]);
    }

    public function delete(Request $request, $id)
    {
        $level = TrackLevels::findOrFail($id);
        $level->delete();
        $request->session()->flash('error', 'Level successfully deleted!');

        return redirect()->route('track-levels.index');
    }

    public function save(Request $request, $id = null)
    {
        $max = $request->input(TrackLevels::FIELD_MAX_LEVEL_WORD);
        Validator::make($request->all(), [
            TrackLevels::FIELD_TRACK_LEVEL => 'sometimes|required|unique:track_levels,track_level,' . $id . ',id',
            TrackLevels::FIELD_MIN_LEVEL_WORD => 'required|numeric|min:1|lt:' . $max,
            TrackLevels::FIELD_MAX_LEVEL_WORD => 'required|numeric|'
        ])->validate();

        TrackLevels::updateOrCreate([TrackLevels::FIELD_ID => $id], $request->all());
        $request->session()->flash('success', $id ? 'Track levels successfully updated' : 'Track levels successfully created');

        return redirect()->route('track-levels.index');
    }
}
