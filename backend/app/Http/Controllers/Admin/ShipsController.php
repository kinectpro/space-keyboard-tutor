<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ShipModels;
use Illuminate\Support\Facades\Input;
use Monolog\Handler\SamplingHandler;
use function PHPSTORM_META\type;
use Session;
use SoftDeletes;

class ShipsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $ships = ShipModels::sortable()->withTrashed()->paginate(5);

        return view('ships.index', [
            'ships' => $ships
        ]);
    }

    public function new()
    {
        return view('ships.new');
    }

    public function edit($id)
    {
        return view('ships.edit', [
            'ship' => ShipModels::withTrashed()->findOrFail($id)
        ]);
    }

    public function save(Request $request, $id = null)
    {
        $this->validate($request, [
            ShipModels::FIELD_NAME => 'required',
            ShipModels::FIELD_LEVEL => 'required',
            ShipModels::FIELD_HEALTH => 'required',
            ShipModels::FIELD_FUEL => 'required',
            ShipModels::FIELD_SILVER => 'integer|nullable',
            ShipModels::FIELD_GOLD => 'integer|nullable',
            ShipModels::FIELD_IMAGE => 'nullable|image'
        ]);

        $input = $request->all();
        if ($request->hasFile('image')) {
            $request->file('image')->storeAs('public/image/ships', $request->file('image')->getClientOriginalName());
            $input[ShipModels::FIELD_IMAGE] = $request->file('image')->getClientOriginalName();
        }

        if (!$id) {
            ShipModels::create($input);
        } else {
            $ship = ShipModels::withTrashed()->findOrFail($id);
            $ship->fill($input);
            $ship->save();
        }

        $request->session()->flash('success', $id  ? 'Ship successfully updated' : 'Ship successfully created');

        return redirect()->route('ships.index');
    }

    public function view($id)
    {
        $ship = ShipModels::withTrashed()->findOrFail($id);
        return view('ships.view', [
            'ship' => $ship
        ]);
    }

    public function delete(Request $request, $id)
    {
        /** @var ShipModels $ship */
        $ship = ShipModels::withTrashed()->findOrFail($id);
        if ($ship->deleted_at) {
            $ship->restore();
            $request->session()->flash('success', 'Ship successfully restored!');
        } else {
            $ship->is_enabled = false;
            $ship->is_published = false;
            $ship->save();
            $ship->delete();
            $request->session()->flash('error', 'Ship successfully deleted!');
        }
        return redirect()->route('ships.index');
    }

    public function publish(Request $request, $id)
    {
        /** @var ShipModels $ship */
        $ship = ShipModels::withTrashed()->findOrFail($id);
        if ($ship->is_published == false) {
            $ship->is_published = true;
            $ship->save();
            $request->session()->flash('success', 'Ship successfully published!');
        } else {
            $ship->is_published = false;
            $ship->save();
            $request->session()->flash('success', 'Ship successfully unpublished!');
        }
        return redirect()->route('ships.index');
    }

    public function enable(Request $request, $id)
    {
        /** @var ShipModels $ship */
        $ship = ShipModels::withTrashed()->findOrFail($id);
        if ($ship->is_enabled == false) {
            $ship->is_enabled = true;
            $ship->save();
            $request->session()->flash('success', 'Ship successfully enabled!');
        } else {
            $ship->is_enabled = false;
            $ship->save();
            $request->session()->flash('success', 'Ship successfully disabled!');
        }
        return redirect()->route('ships.index');
    }
}
