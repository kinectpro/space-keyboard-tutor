<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Level;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Monolog\Handler\SamplingHandler;
use function PHPSTORM_META\type;
use Session;

/**
 * Class LevelsController
 * @package App\Http\Controllers\Admin
 */
class LevelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $levels = Level::sortable()->paginate(5);

        return view('levels.index', [
            'levels' => $levels
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        return view('levels.edit');
    }

    public function view($id)
    {
        return view('levels.details', [
            'level' => Level::findOrFail($id)
        ]);
    }

    public function edit($id)
    {
        $level = Level::findOrFail($id);

        return view('levels.edit', [
            'level' => $level
        ]);
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request, $id = null)
    {
        $this->validate($request, [
            Level::FIELD_LEVEL => 'required|integer',
            Level::FIELD_EXPERIENCE => 'required|integer',
            Level::FIELD_MIN_LENGTH => 'required|integer',
            Level::FIELD_MAX_LENGTH => 'required|integer'
        ]);

        /** @var Level $level */
        $level = Level::firstOrNew([Level::FIELD_ID => $id]);
        $level->fill($request->all());
        if ($level->min_length > $level->max_length)
        {
            $request->session()->flash('error', 'Maximum Word length cant be smaller then Minimum Word length!');
            return redirect()->back();
        }

        $level->save();

        $request->session()->flash('success', $id ? 'Level successfully updated' : 'Level successfully created');
        return redirect()->route('levels.index', $request->query());
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $id)
    {
        $level = Level::findOrFail($id);
        $level->delete();
        $request->session()->flash('success', 'Level successfully deleted!');

        return redirect()->route('levels.index', $request->query());
    }
}
