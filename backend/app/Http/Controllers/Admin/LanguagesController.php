<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\Word;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

/**
 * Class LanguagesController
 * @package App\Http\Controllers\Admin
 */
class LanguagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $languages = Language::sortable()->withTrashed()
            ->paginate(10);


        return view('languages.index', [
            'languages' => $languages
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id)
    {
        $language = Language::withTrashed()->findOrFail($id);

        return view('languages.details', [
            'language' => $language
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $language = Language::withTrashed()->findOrFail($id);

        return view('languages.edit', [
            'language' => $language
        ]);
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     * @throws
     */
    public function delete(Request $request, $id)
    {
        /** @var Language $language */
        $language = Language::withTrashed()->findOrFail($id);
        if ($language->trashed()) {
            $language->restore();
            $request->session()->flash('success', 'Language successfully restored!');
        } else {
            $language->delete();
            $request->session()->flash('success', 'Language successfully deleted!');
        }

        return redirect()->route('languages.index', $request->query());
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     * @throws
     */
    public function update(Request $request, $id = null)
    {
        $this->validate($request, [
            Language::FIELD_NAME => 'required|unique:languages'
        ]);

        $language = Language::firstOrNew([Word::FIELD_ID => $id]);
        $language->fill($request->all());
        $language->save();

        $request->session()->flash('success', $id  ? 'Language successfully updated' : 'Language successfully created');

        return redirect()->route('languages.index', $request->query());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        return view('languages.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fromFile(Request $request, $id = null)
    {
        if (!Language::all()->count()) {
            $request->session()->flash('error', 'No Languages Found!');
            return redirect()->route('languages.index');
        }
        if ($id && !Language::find($id)) {
            $request->session()->flash('error', 'Language Not Found!');
            return redirect()->route('languages.index');
        }
        $languages = Language::all();

        return view('languages.upload', [
            'languages' => $languages,
            'option_id' => $id
        ]);
    }


    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function upload(Request $request)
    {

        $this->validate($request, [
            Language::FIELD_ID => 'required',
            'dictionary' => 'required|mimes:txt'
        ]);

        $id = $request->get(\App\Language::FIELD_ID);

        if (!$id) {
            $request->session()->flash('error', 'Select Language !');
            return redirect()->route('languages.from-file');
        }
        if (!($request->hasFile('dictionary'))) {
            $request->session()->flash('error', 'No file selected !');
            return redirect()->route('languages.from-file');
        }

        $request->file('dictionary')->storeAs('dictionaries', $request->file('dictionary')->getClientOriginalName());
        $name = $request->file('dictionary')->getClientOriginalName();

        $contents = Storage::get('dictionaries/' . $name);
        $words = preg_split('($|\s)', $contents);

        foreach ($words as $word) {
            if (!strlen($word)) {
                continue;
            }
            if (Word::where(Word::FIELD_LANGUAGE_ID, $id)->where(Word::FIELD_WORD, $word)->exists()) {
                continue;
            }
            Word::create([
                Word::FIELD_LANGUAGE_ID => $id,
                Word::FIELD_WORD => trim(strtolower($word))
            ]);
        }
        $request->session()->flash('success', 'Word uploaded!');

        return redirect()->route('languages.index');
    }
}
