<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Word;
use App\Language;

/**
 * Class WordsController
 * @package App\Http\Controllers\Admin
 */
class WordsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $language_id = $request->get(Word::FIELD_LANGUAGE_ID);
        $languages = Language::withTrashed()->get();

        $dictionaries = Word::when($language_id, function ($query, $language_id) {
            return $query->where(Word::FIELD_LANGUAGE_ID, $language_id);
        })->sortable()->paginate(10);

        return view('dictionaries.index', [
            'languages' => $languages,
            'dictionary' => $dictionaries,
            'option_id' => $language_id
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id)
    {
        return view('dictionaries.details', [
            'word' => Word::findOrFail($id)
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $word = Word::findOrFail($id);
        $languages = Language::all();

        return view('dictionaries.edit', [
            'word' => $word,
            'languages' => $languages,
            'language_id' => $word->language_id
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View | \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     * @throws
     */
    public function updateOrCreate(Request $request, $id = null)
    {
        $this->validate($request, [
            Word::FIELD_LANGUAGE_ID => 'required',
            Word::FIELD_WORD => 'required|unique:words|regex:/^\S*$/u'
        ]);

        /** @var Word $word */
        $word = Word::firstOrNew([Word::FIELD_ID => $id]);
        $word->fill($request->all());
        $word->word = trim(strtolower($word->word));
        $word->save();

        $request->session()->flash('success', $id ? 'Word successfully updated' : 'Word successfully created');

        return redirect()->route('dictionary.index', $request->query());
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $id)
    {
        $word = Word::findOrFail($id);
        $word->delete();
        $request->session()->flash('success', 'Word successfully deleted!');

        return redirect()->route('dictionary.index', $request->query());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function new(Request $request)
    {
        if (!Language::all()->count()) {
            $request->session()->flash('error', 'No Languages Found!');
            return redirect()->route('dictionary.index');
        }
        $languages = Language::all();

        return view('dictionaries.edit', [
            'languages' => $languages,
            'language_id' => 0
        ]);
    }
}