<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $email = $request->query->get('filter_email', '');
        $nickname = $request->query->get('filter_nickname', '');

        $users = User::withTrashed()
            ->when($email, function ($query, $email) {
                return $query->where(User::FIELD_EMAIL, 'like', '%' . $email . '%');
            })->when($nickname, function ($query, $nickname) {
                return $query->where(User::FIELD_NICKNAME, 'like', '%' . $nickname . '%');
            })
            ->sortable()
            ->paginate(10);

        return view('users.index', [
            'users' => $users
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function view($id)
    {
        /** @var User $user */
        $user = User::withTrashed()->findOrFail($id);

        return view('users.details', [
            'user' => $user
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function edit($id)
    {
        $user = User::withTrashed()->findOrFail($id);

        return view('users.edit', [
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(Request $request, $id)
    {
        /** @var User $user */
        $user = User::withTrashed()->findOrFail($id);

        Validator::make($request->all(), [
            User::FIELD_EMAIL => 'required|email',
            User::FIELD_NICKNAME => 'required',
            User::FIELD_EXPERIENCE => 'required|integer|min:0',
            User::FIELD_DOCK_SIZE => 'required|integer|min:1',
            User::FIELD_PASSWORD => 'nullable|min:4'
        ])->validate();

        $user->fill($request->all([
            User::FIELD_EMAIL,
            User::FIELD_NICKNAME,
            User::FIELD_EXPERIENCE,
            User::FIELD_DOCK_SIZE
        ]));

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        $user->save();
        $request->session()->flash('success', 'User ' . $user->email . ' successfully updated');

        return redirect()->route('users.index');
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function banOrUnban(Request $request, $id)
    {
        /** @var User $user */
        $user = User::withTrashed()->findOrFail($id);

        if ($user->banned_at) {
            $user->banned_at = null;
            $user->unban_at = null;
            $user->save();
            $request->session()->flash('success', 'User successfully unbanned');

            return redirect()->route('users.index', $request->query());
        }

        $validator = Validator::make($request->all(), [
            'banUpTo' => 'nullable|date',
            'banPeriod' => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('error', $validator->errors()->first());

            return redirect()->route('users.index', $request->query());
        }

        $user->banned_at = Carbon::now();
        if ($request->input('banUpTo')) {
            $user->unban_at = Carbon::createFromFormat('Y-m-d', $request->input('banUpTo'));
        } else {
            $user->unban_at = $request->input('banPeriod') ? Carbon::now()->addDays(($request->input('banPeriod'))) : null;
        }

        $user->save();
        $request->session()->flash('success', 'User successfully banned');

        return redirect()->route('users.index', $request->query());
    }

    /**
     * @param integer $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     * @throws
     */
    public function deleteOrRestore(Request $request, $id)
    {
        /** @var User $user */
        $user = User::withTrashed()->findOrFail($id);

        if ($user->deleted_at) {
            $user->restore();
        } else {
            $user->delete();
        }

        return redirect()->route('users.index', $request->query());
    }

}
