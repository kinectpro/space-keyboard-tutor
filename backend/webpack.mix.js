const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/admin/index.js', 'public/js/admin')
    .js('resources/js/admin/users/index.js', 'public/js/admin/users')
    .js('resources/js/admin/ships/index.js', 'public/js/admin/ships')
    .js('resources/js/admin/languages/index.js', 'public/js/admin/languages')
    .js('resources/js/admin/dictionaries/index.js', 'public/js/admin/dictionaries')
    .js('resources/js/admin/track-levels/index.js', 'public/js/admin/track-levels')
    .sass('resources/sass/app.scss', 'public/css');
