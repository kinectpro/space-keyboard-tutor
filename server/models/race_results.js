'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('race_results', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        ship_id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        experience: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        gold: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        silver: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        place: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        time_lap: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
    }, {
        timestamps: false
    });
};
