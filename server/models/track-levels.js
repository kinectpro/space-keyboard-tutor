'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('track_levels', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        track_level: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        min_level_word: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        max_level_word: {
            type: DataTypes.INTEGER,
            primaryKey: true
        }
    }, {
        timestamps: false
    });
};