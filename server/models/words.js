'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('words', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        word: {
            type: DataTypes.STRING,
            primaryKey: true
        },
    }, {
        timestamps: false
    });
};
