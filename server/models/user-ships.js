'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user_ships', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        user_id : {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        ship_model_id : {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        health : {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
    }, {
        timestamps: false
    });
};
