'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        nickname: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        gold: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        silver: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        experience: {
            type: DataTypes.INTEGER,
            primaryKey: true
        }
    }, {
        timestamps: false
    });
};
