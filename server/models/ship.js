'use strict';

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('ship_models', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        level: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        fuel: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        health : {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
    }, {
        timestamps: false
    });
};
