'use strict';

const action = require('./config/enum').action;
const message = require('./config/enum').msg;
const error = require('./config/enum').error;
const constNum = require('./config/enum').constNum;
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const conf = require('./config/conf.json');
const getUserShip = require('./modules/ships/get-user-ship.module');
const Rooms = require('./modules/rooms/rooms');

const rooms = new Rooms;

let sockets = {};
let userId;

io.on(action.connection, socket => {
    console.log('a user connected: ' + socket.id);
    socket.emit(action.connection, message.connectionSuccess);

    socket.on(action.join, async (inputInfo, res) => {
        let currentRoom, userShip;

        if (inputInfo) {
            sockets[userId] = inputInfo;
        } else if (sockets[userId] && sockets[userId].socket) {
            sockets[userId].socket.disconnect(true);
        }
        if (!sockets[userId].shipId) {
            sockets.emit(error.noUserShip, {success: false, msg: 'shipEmpty'});
            socket.disconnect(true);
            return;
        }
        sockets[userId].socket = socket;
        let currentData = rooms.findUsersInRooms({userId: userId, shipId: sockets[userId].shipId});
        userShip = currentData.userShip ? currentData.userShip : await getUserShip(userId, sockets[userId].shipId);
        currentRoom = currentData.userShip ? currentData.currentRoom : rooms.addUserInRooms(userShip);

        socket.join(currentRoom.name);
        socket.broadcast.emit(action.newPlayer, userShip);

        const timer = setInterval(() => {
            let timeToStart = currentRoom.timeToStart();
            if(timeToStart === 3 && currentRoom.ships.length < constNum.MAX_SIZE_ROOM) {
                currentRoom.state = 'race';
                while(currentRoom.ships.length < constNum.MAX_SIZE_ROOM) {
                    io.to(currentRoom.name).emit(action.newPlayer, currentRoom.addBot());
                }
            }
            if (timeToStart <= 0) {
                startGame();
                clearInterval(timer);
            }
        }, constNum.MILLISECONDS_IN_SECONDS);

        socket.broadcast.emit(action.newPlayer, {
            ship: userShip,
        });
        console.log('user joined at ' + currentRoom.name);

        res({
            success: true,
            msg: {
                level: currentRoom.level,
                traceLen: currentRoom.traceLen,
                currentUserId: userId,
                currentShipId: sockets[userId].shipId,
                ships: currentRoom.ships,
                timeToStart: currentRoom.timeToStart()
            }
        });

        socket.on(action.word, (data, res) => {
            let isRightInputWord = currentRoom.checkWord(userShip, data.word);
            isRightInputWord ? currentRoom.rightAnswer(userShip, data.word) : currentRoom.wrongAnswer(userShip);
            io.to(currentRoom.name).emit(action.updateRoom, currentRoom.ships);
            let msg;
            let stc;
            if (currentRoom.isFinish(userShip)) {
                let place = currentRoom.getPlace;
                msg = {
                    isRightInputWord: isRightInputWord,
                    isFinish: true,
                    place: place,
                    silver: currentRoom.getSilver(place),
                    gold: currentRoom.getGold(place),
                    exp: currentRoom.getExp(place, userShip)
                };

                stc = {
                    user_id: userShip.user_id,
                    ship_id: userShip.id,
                    experience: currentRoom.getExp(place, userShip),
                    gold: currentRoom.getGold(place),
                    silver: currentRoom.getSilver(place),
                    place: place,
                    time_lap: currentRoom.getTimeLap()
                };
                console.log(stc);
                userShip.updateUser(msg);
                userShip.updateUserShip();
                userShip.createStatistic(stc);
            } else {
                msg = {
                    isRightInputWord: isRightInputWord,
                    isFinish: false,
                }
            }
            res({
                success: true,
                msg
            })
        });

        socket.on(action.getWord, (data, res) => {
            res({
                success: true,
                msg: {
                    isFinish: currentRoom.isFinish(userShip),
                    prevWord: currentRoom.prevWord(userShip),
                    word: currentRoom.newWord(userShip),
                    nextWord: currentRoom.nextWord(userShip),
                    ships: currentRoom.ships
                }
            });
        });

        function update() {
            let timer = setInterval(() => {
                currentRoom.updateBot();
                io.to(currentRoom.name).emit(action.updateRoom, currentRoom.ships);
                if (rooms.checkRoomToFinalAll(currentRoom)) {
                    clearInterval(timer);
                }

            }, 2000);
        }

        function startGame() {
            update();
            res = {
                ships: currentRoom.ships,
            };
            io.to(currentRoom.name).emit(action.start, res);
        }

    });

    io.on(action.disconnect, function () {
        console.log('User disconnected game; ' + socket.id);
        io.emit(action.disconnect);
    });
});

io.use((socket, next) => {
    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, conf.secret, function (err, decoded) {
            if (err) return next(new Error('Authentication error'));
            socket.decoded = decoded;
            userId = decoded.sub;
            next();
        });
    } else {
        next(new Error('Authentication error'));
    }
});

app.use(function (req, res, next) {
    let err = new Error(error.notFound);
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500).json({
        message: err.message,
        error: err
    });
});

server.listen(conf.port, function () {
    console.log('listening on *:3000');
});
