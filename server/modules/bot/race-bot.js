"use strict";
const confEnum = require('../../config/enum');
const constNum = confEnum.constNum;

class TraceBotModule {
    constructor(traceLevel, traceLen) {
        this.isBot = true;
        this.botStartTime = new Date().getTime();
        this.userShip = this.getUserShip(traceLevel);
        this.traceLen = traceLen;
        this.name = this.userShip.name;
        this.id = this.userShip.id;
        this.user_id = this.userShip.user_id;
        this.nickname = this.userShip.nickname;
        this.level = traceLevel;
        this.health = this.userShip.health;
        this.maxFuel = this.userShip.maxFuel;
        this.fuel = 0;
        this.refuelingProgress = 0;
        this.raceProgress = 0;
        this.state = 'wait';
        this.wordsWithErrors = [];
        this.currentNum = 0;
        this.speedInputWord = traceLevel * this.randomInteger(10, 100);
        this.inputWord = 0;
        console.log('this', this);
    }

    randomInteger(min, max) {
        return Math.round(min - 0.5 + Math.random() * (max - min + 1));
    }

    getUserShip(traceLevel) {
        return {
            name: 'ShipName'+this.randomInteger(100, 200),
            id: this.randomInteger(100, 200),
            user_id: this.randomInteger(100, 200),
            nickname: 'BotNickName'+this.randomInteger(100, 200),
            level: traceLevel,
            health: this.randomInteger(10, 200),
            maxFuel: '200',
        };
    }

    updateBotProgress() {
        this.raceProgress = (new Date().getTime() - this.botStartTime) / (50 * this.speedInputWord) * this.level;
        this.raceProgress = this.raceProgress > this.traceLen ? this.traceLen : this.raceProgress;
        if (this.raceProgress >= this.traceLen) {
            this.state = 'finish';
        }
    }
}

module.exports = TraceBotModule;
