"use strict";

const Room = require('./room.js');
const state = require('../../config/enum').state;
const Dictionary = require('../dictionary/dictionary.module');

class Rooms {
    constructor() {
        this.rooms = [];
        this.dictionary = new Dictionary();
    }

    addUserInRooms(userShip) {
        this.userShip = userShip;
        let room = this.findFreeRooms();
        return !room ? this.createNewRoom() : room;
    }

    createNewRoom() {
        let room = new Room(this.userShip, this.rooms.length);
        room.dictionary = this.dictionary.getWordsByLevel(room.level);
        console.log(room.dictionary);
        this.rooms.push(room);
        return room;
    }

    findFreeRooms() {
        for (let room of this.rooms) {
            if (room.addUser(this.userShip)) {
                return room;
            }
        }
        return false;
    }

    findUsersInRooms(data) {
        for (let room of this.rooms) {
            for (let ship of room.ships) {
                if (ship.user_id === data.userId
                    && ship.shipModelId === data.shipId
                    && room.state !== state.closed
                    && ship.state !== 'finish'
                    && ship.state !== 'destroy'
                ) {
                    return {userShip: ship, currentRoom: room};
                }
            }
        }
        return false;
    }

    checkRoomToFinalAll(room) {
        for (let ship of room.ships) {
            if (ship.state !== state.finish) {
                return false;
            }
        }
        room.state = state.closed;
        this.saveUserShip(room);
        return true;
    }

    saveUserShip(room) {
        for (let ship of room.ships) {
            if (!ship.isBot) {
                ship.updateUserShip();
            }
        }
    }
}

module.exports = Rooms;
