"use strict";

let Room = require('./Room.js');

class Rooms {
    constructor() {
        this.rooms = [];
    }
    addUserInRooms(userShip) {
        let roomName = this.findFreeRooms(userShip);
        if(!roomName) {
            roomName = this.createNewRoom(userShip);
        }
        return roomName;
    }
    createNewRoom(userShip) {
        let count = 0;
        if (this.rooms.length) {
            count = this.rooms.length;
        }
        let room = new Room(userShip, count);
        this.rooms.push(room);
        return room.name;
    }
    findFreeRooms(userShip) {
        for(let room of this.rooms) {
            if(room.addUser(userShip)) {
                return room.name;
            }
        }
        return false;
    }
}

module.exports = Rooms;
