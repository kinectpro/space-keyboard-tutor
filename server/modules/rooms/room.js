"use strict";

const getDictionary = require('../dictionary/get-dictionary');
const constNum = require('../../config/enum').constNum;
const RaceBot = require('../bot/race-bot');
const state = require('../../config/enum').state;

class Room {
    constructor(userShip, currentRoomId = 0) {
        this.ships = [];
        this.name = 'room_' + currentRoomId;
        this.level = userShip.level;
        this.langId = 1;
        this.ships.push(userShip);
        this.state = state.wait;
        this.timeCreat = new Date().getTime();
        this.timeClose = new Date().getTime() + constNum.MAX_TIME_RACES * constNum.MILLISECONDS_IN_SECONDS;
        this.traceLen = constNum.COEFFICIENT_TRACE_LEN * this.level;
        this.results = [];
        this.dictionary = [];
        setTimeout(() => this.state = state.closed, (constNum.MAX_TIME_RACES + constNum.TIME_TO_START) * constNum.MILLISECONDS_IN_SECONDS);
    }

    addUser(userShip) {
        if (userShip.level === this.level && this.checkCount() && this.state === state.wait) {
            this.ships.push(userShip);
            return true;
        } else {
            return false;
        }
    }

    checkCount() {
        return this.ships.length < constNum.MAX_SIZE_ROOM;
    }

    newWord(userShip) {
        return this.dictionary[userShip.currentNum].word;
    }

    prevWord(userShip) {
        return this.dictionary[userShip.currentNum - 1] ? this.dictionary[userShip.currentNum - 1].word : null;
    }

    nextWord(userShip) {
        // тут иногда TypeError: Cannot read property 'word' of undefined. пока непонятно почему
        return this.dictionary[userShip.currentNum + 1].word;
    }

    checkWord(userShip, inputWord) {
        return this.dictionary[userShip.currentNum++].word === inputWord;
    }

    wrongAnswer(userShip) {
        userShip.wordsWithErrors.push(this.dictionary[userShip.currentNum - 1].word);
        userShip.health = userShip.health - 5 * userShip.wordsWithErrors.length * this.level;
        if (userShip.health <= 0) {
            userShip.health = 0;
            userShip.state = state.destroy;
            userShip.updateUserShip();
        }
    }

    rightAnswer(userShip, word) {
        userShip.raceProgress += word.length;
    }

    timeToStart() {
        return (constNum.TIME_TO_START - Math.round((new Date().getTime() - this.timeCreat) / constNum.MILLISECONDS_IN_SECONDS));
    }

    isFinish(userShip) {
        if (userShip.raceProgress >= this.traceLen) {
            userShip.state = state.finish;
            return true;
        }
        return false;
    }

    addBot() {
        const bot = new RaceBot(this.level, this.traceLen);
        this.ships.push(bot);
        return bot;
    }

    updateBot() {
        for (let ship of this.ships) {
            if (ship.isBot && ship.state !== state.finish) {
                ship.updateBotProgress()
            }
        }
    }

    get getPlace() {
        let place = 0;
        for (const ship of this.ships) {
            if (ship.state === state.finish) {
                place++;
            }
        }
        return place;
    }

    getSilver(place) {
        return Math.round(100 * this.level / place);
    }

    getExp(place, userShip) {
        return Math.round(this.traceLen - userShip.getLenWordWithErrors * constNum.COEFFICIENT_WRONG_WORD_EXP);
    }

    getGold(place) {
        return place === 1 && !Math.floor(Math.random() * (20 - 1)) ? 10 * this.level : 0;
    }

    getTimeLap(){
        return new Date().getTime() - this.timeCreat - this.timeToStart();
    }
}

module.exports = Room;
