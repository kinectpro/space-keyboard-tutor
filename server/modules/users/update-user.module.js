'use strict';

module.exports = (ship, data) => {
    const models = require('../../models');
    const users = models.users;
    users.update({
            gold: data.gold + ship.gold,
            silver: data.silver + ship.silver,
            experience: data.exp + ship.experience
        }, {
            where: {
                id: ship.user_id
            }
        }
    );
};