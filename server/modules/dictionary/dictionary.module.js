const getDictionary = require('./get-dictionary');
const getTrackLevels = require('./get-track-levels');

class DictionaryModule {
    constructor() {
        this.getDictionary();
    }

    async getDictionary() {
        this.dictionary = await getDictionary();
        this.trackLevels = await getTrackLevels();

        let newDictionary = [];

        for (let level of this.trackLevels) {
            let words = [];
            for (let word of this.dictionary) {
                let wordLen = word.word.length;
                if (wordLen >= level.min_level_word && wordLen <= level.max_level_word) {
                    words.push(word);
                }
            }
            newDictionary.push(words);
        }
        this.dictionary = newDictionary;
    }

    getWordsByLevel(level) {
        return this.shuffle(this.dictionary[level - 1]);
    }

    shuffle(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}

module.exports = DictionaryModule;
