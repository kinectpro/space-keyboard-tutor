'use strict';

module.exports = (level, langId) => {
    const models = require('../../models/index');

    return new Promise((resolve) => {
        models.words.findAll().then((dictionary) => {
            resolve(JSON.parse(JSON.stringify(dictionary)));
        })
    });
};
