'use strict';

module.exports = () => {
    const models = require('../../models/index');

    return new Promise((resolve) => {
        models.track_levels.findAll().then((trackLevels) => {
            resolve(JSON.parse(JSON.stringify(trackLevels)));
        })
    });
};
