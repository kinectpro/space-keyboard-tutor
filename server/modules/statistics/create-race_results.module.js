'use strict';

module.exports = (ship, data) => {

    const models = require('../../models');
    const race_results = models.race_results;

    console.log(data);

    race_results.create({
        user_id: data.user_id,
        ship_id: data.ship_id,
        experience: data.experience,
        gold: data.gold,
        silver: data.silver,
        place: data.place,
        time_lap: data.time_lap
        }
    );
};