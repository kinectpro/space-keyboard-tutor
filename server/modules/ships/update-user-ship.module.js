'use strict';

module.exports = userShip => {

    const models = require('../../models');
    const userShips = models.user_ships;

    userShips.update({
            health: Math.round(userShip.health)
        }, {
            where: {
                id: userShip.id
            }
        }
    );
};