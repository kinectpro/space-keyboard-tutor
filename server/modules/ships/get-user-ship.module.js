'use strict';

const UserShips = require('./user-ship.module');
module.exports = (userId, shipId) => {
    const models = require('../../models');

    const user = models.users;
    const userShips = models.user_ships;
    const ship = models.ship_models;

    userShips.belongsTo(user, {foreignKey: 'user_id'});
    userShips.belongsTo(ship, {foreignKey: 'ship_model_id'});

    return new Promise((resolve) => {
        userShips.findOne({
            where: {
                user_id: userId,
                ship_model_id: shipId
            },
            include: [user, ship]
        }).then((userShip) => {
            userShip = new UserShips(userShip.toJSON());
            resolve(userShip);
        });
    });
};
