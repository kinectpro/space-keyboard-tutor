"use strict";
const updateUser = require('../users/update-user.module');
const createStatistic = require('../statistics/create-race_results.module');
const updateUserShip = require('../ships/update-user-ship.module');
class UserShipModule {
    constructor(userShip) {
        this.name = userShip.ship_model.name;
        this.nickname = userShip.user.nickname;
        this.id = userShip.id;
        this.level = userShip.ship_model.level;
        this.user_id = userShip.user_id;
        this.health = userShip.health;
        this.maxFuel = userShip.ship_model.fuel;
        this.fuel = 0;
        this.raceProgress = 0;
        this.refuelingProgress = 0;
        this.state = 'wait';
        this.currentNum = 0;
        this.wordsWithErrors = [];
        this.shipModelId = userShip.ship_model.id;
        this.healthMax = userShip.ship_model.health;
        this.gold = userShip.user.gold;
        this.silver = userShip.user.silver;
        this.experience = userShip.user.experience;
    }

    get userShip() {
        return this;
    }

    set progress(value) {
        this.raceProgress = value;
    }

    get progress() {
        return this.raceProgress;
    }

    get getLenWordWithErrors() {
        return this.wordsWithErrors.length ? this.wordsWithErrors.reduce((a, b) => a + b, 0).length : 0;
    }

    updateUser(data) {
        updateUser(this, data);
    }

    createStatistic(data) {
        createStatistic(this, data);
    }

    updateUserShip() {
        updateUserShip(this);
    }
}

module.exports = UserShipModule;
