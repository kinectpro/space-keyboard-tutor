var models = require('../models');

exports.randomInteger = function (min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

exports.getDictionary = function () {
    var dictionaries = [];
    models.Dictionary.findAll()
        .then(results => {
            results.forEach(function (result) {
                if (result.word.length < 5 ) {
                    result.difficulty_level = 0;
                }
                if (result.word.length >= 5 &&  result.word.length <8) {
                    result.difficulty_level = 1;
                }
                if (result.word.length >= 8 &&  result.word.length < 11 ) {
                    result.difficulty_level = 2;
                }
                if (result.word.length >= 11 &&  result.word.length < 15) {
                    result.difficulty_level = 3;
                }
                if (result.word.length >= 15) {
                    result.difficulty_level = 4;
                }
                if (dictionaries[result.difficulty_level] === undefined) {
                    dictionaries[result.difficulty_level] = [];
                }
                dictionaries[result.difficulty_level].push(result.word);
            });
        });
    return dictionaries;
};
