exports.constNum = {
  'MAX_SIZE_ROOM': 4,
  'MAX_TIME_RACES': 300,
  'TIME_TO_START': 10,
  'COEFFICIENT_TRACE_LEN': 10,
  'MILLISECONDS_IN_SECONDS': 1000,
  'COEFFICIENT_WRONG_WORD_EXP': 0.5,
};
exports.action = {
  'room': 'room',
  'connection': 'connection',
  'join': 'join',
  'updateRoom': 'update room',
  'timeToStart': 'time to start',
  'start': 'start',
  'getWord': 'get word',
  'word': 'word',
  'disconnect': 'disconnect',
  'results': 'results',
  'newPlayer': 'new player'
};
exports.msg = {
  'connectionSuccess': 'connection success',
  'userJoined': 'user joined at',
  'finished': 'track is finished',
  'invalidToken': 'invalid token...'
};
exports.error = {
  'notFound': 'Not Found',
  'unauthorizedError': 'UnauthorizedError',
    'noUserShip': 'no user ship'
};
exports.state = {
  'race': 'race',
    'wait': 'wait',
    'finish': 'finish',
    'closed': 'closed',
    'isFinishNow': 'isFinishNow',
    'destroy': 'destroy'
};