import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: './main/main.module#MainModule',
  },

  {
    path: 'track',
    loadChildren: './track/track.module#TrackModule',
    canActivate: [AuthGuard]
  },

  {
    path: 'dock',
    loadChildren: './dock/dock.module#DockModule',
    canActivate: [AuthGuard]
  },

  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule',
    canActivate: [AuthGuard]
  },

  {
    path: 'score',
    loadChildren: './score/score.module#ScoreModule',
    canActivate: [AuthGuard]
  },

  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },

  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
