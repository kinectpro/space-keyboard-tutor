import {FormGroup} from '@angular/forms';

export class PasswordValidator {
  static validate(registrationFormGroup: FormGroup) {
    const password = registrationFormGroup.controls.password.value;
    const repeatPassword = registrationFormGroup.controls.confirmPassword.value;

    if (!repeatPassword.length || repeatPassword === password) {
      return null;
    }

    registrationFormGroup.controls.confirmPassword.setErrors({
      notMatch: true
    })
  }
}