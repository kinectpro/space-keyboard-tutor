import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  code: string | null;
  loaded: boolean = false;
  success: boolean | null = null;
  error: string;

  constructor(private route: ActivatedRoute, private auth: AuthService, private spinner: NgxSpinnerService) {
    this.code = this.route.snapshot.params.key;
  }

  ngOnInit() {
    this.spinner.show();

    this.auth.confirm(this.code).subscribe(() => {
      this.spinner.hide();
      this.loaded = true;
      this.success = true;
    }, error => {
      this.spinner.hide();
      this.loaded = true;
      this.success = false;
    })
  }
}
