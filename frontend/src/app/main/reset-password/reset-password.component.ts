import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {PasswordValidator} from "../helpers/password-validator";
import {NgxSpinnerService} from "ngx-spinner";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [AuthService],
})
export class ResetPasswordComponent implements OnInit {

  key: string | null;
  successChange: boolean | null = null;
  loaded: boolean = false;
  success: boolean | null = null;
  error: string;

  form = this.formBuilder.group({
    password: ['', [Validators.required, Validators.minLength(environment.passwordMinLength)]],
    confirmPassword: ['', Validators.required],
  }, {
    validator: PasswordValidator.validate.bind(this)
  });

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private auth: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.key = this.route.snapshot.params.key;
  }

  ngOnInit() {
    this.spinner.show();
    this.auth.validatePasswordToken(this.key)
      .pipe(finalize(() => {
        this.spinner.hide();
        this.loaded = true;
      })).subscribe(() => {
      this.success = true;
    }, error => {
      this.error = error.error.email;
    })
  }

  onSubmit() {
    this.form.value['token'] = this.key;
    this.auth.changePassword(this.form.value).subscribe(() => {
      this.successChange = true;
    }, error => {
      this.success = false;
      this.error = error.error.email;
    });

  }

}
