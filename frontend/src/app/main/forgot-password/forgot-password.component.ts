import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [AuthService]
})
export class ForgotPasswordComponent implements OnInit {

  error: string;
  onLoaded: boolean = true;
  successSent: boolean = false;
  form = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
  });

  get email() {
    return this.form.get('email');
  }

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.onLoaded = false;
    this.spinner.show();
    this.error = '';
    this.auth.forgotPassword(this.form.value).subscribe(() => {
      this.onLoaded = true;
      this.successSent = true;
      this.spinner.hide();
    }, error => {
      this.onLoaded = true;
      this.error = error.error.email;
      this.spinner.hide();
    });

  }
}
