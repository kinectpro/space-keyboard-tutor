import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {PasswordValidator} from '../helpers/password-validator';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  error: string;
  registrationSuccess: boolean = false;

  form = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(environment.passwordMinLength)]],
    confirmPassword: ['', Validators.required]
  }, {
    validator: PasswordValidator.validate.bind(this)
  });

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit() {
    console.log(this.password.errors, this.confirmPassword.errors);

  }

  onSubmit() {
    this.spinner.show();
    this.auth.register(this.form.value).subscribe(() => {
      this.spinner.hide();
      this.registrationSuccess = true;
    }, error => {
      this.error = error.error.email;
    });

  }

}
