import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})

export class LoginComponent implements OnInit {

  error: string;

  form = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(environment.passwordMinLength)]],
    remember_me: [''],
  });

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.spinner.show();
    this.error = '';
    this.auth.login(this.form.value)
      .subscribe(data => {
        this.auth.saveToken(data.token);
        this.router.navigate(['dock']);
      }, error => {
        this.spinner.hide();
        this.error = error.error.email;
      });

  }
}
