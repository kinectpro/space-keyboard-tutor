import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainRoutingModule} from './main-routing.module';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {MainComponent} from './main.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmComponent} from './confirm/confirm.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {LogoutComponent} from './logout/logout.component';
import {SharedModule} from '../shared-modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MainRoutingModule,
    NgxSpinnerModule,
    SharedModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    MainComponent,
    ConfirmComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    LogoutComponent
  ],
})
export class MainModule {
}
