import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {MainComponent} from './main.component';
import {ConfirmComponent} from './confirm/confirm.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {GuestGuard} from '../guards/guest.guard';
import {AuthGuard} from '../guards/auth.guard';
import {LogoutComponent} from './logout/logout.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [GuestGuard]
      },

      {
        path: 'register',
        component: RegisterComponent,
        canActivate: [GuestGuard]
      },

      {
        path: 'confirm/:key',
        component: ConfirmComponent
      },

      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },

      {
        path: 'reset-password',
        component: ResetPasswordComponent
      },

      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        canActivate: [GuestGuard]
      },

      {
        path: 'forgot/:key',
        component: ResetPasswordComponent
      },

      {
        path: 'logout',
        component: LogoutComponent,
        canActivate: [AuthGuard]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
