export enum enShipFilter {
  favorite = 'favorite',
  inHangar = 'inHangar',
  available = 'available',
  all = 'all'
}
export enum enShipOrder {
  levelAsc = 'idUp',
  levelDesc = 'idDown'
}
export enum enConstNum {
  RATING_COUNT = 10,
}
export enum enPeriod {
  WEEK = 'Week',
  DAY = 'Day',
  MONTH = 'Month',
  ALL = 'All'
}



export enum state {
  destroy = 'destroy',
  finish = 'finish',
  start = 'start',
  race = 'race',
  move = 'move',
  stop = 'stop'
}