export interface UserInterface {
  readonly nickname: string;
  readonly email: string;
  readonly id: number;
  readonly experience: number;
  readonly dock_size: number;
  readonly silver: number;
  readonly gold: number;
}

export class User implements UserInterface {
    readonly nickname: string;
    readonly email: string;
    readonly id: number;
    readonly experience: number;
    readonly dock_size: number;
    readonly silver: number;
    readonly gold: number;

  constructor(data: UserInterface) {
    this.nickname = data.nickname;
    this.email = data.email;
    this.id = data.id;
    this.experience = data.experience;
    this.dock_size = data.dock_size;
    this.silver = data.silver;
    this.gold = data.gold;
  }
}
