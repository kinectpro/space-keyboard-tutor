export interface ShipInterface {
  readonly created_at: string;
  readonly image: string;
  readonly id: number;
  readonly name: string;
  readonly silver: number;
  readonly level: number;
  readonly fuel: number;
  readonly health: number;
  readonly user_ship: any | null;
  readonly is_published: number;
  readonly is_enabled: number;
  favorite: boolean;
  is_show: boolean;
}

export class Ship implements ShipInterface {
  readonly created_at: string;
  readonly image: string;
  readonly id: number;
  readonly name: string;
  readonly silver: number;
  readonly level: number;
  readonly fuel: number;
  readonly health: number;
  readonly is_published: number;
  readonly is_enabled: number;
  readonly user_ship: any | null;
  favorite: boolean;
  is_favorite: boolean;
  is_show = true;

  constructor(data: ShipInterface) {
    this.created_at = data.created_at;
    this.image = data.image;
    this.id = data.id;
    this.name = data.name;
    this.silver = data.silver;
    this.level = data.level;
    this.fuel = data.fuel;
    this.health = data.health;
    this.is_published = data.is_published;
    this.is_enabled = data.is_enabled;
    this.user_ship = data.user_ship;
    this.is_favorite = data.user_ship && data.user_ship.favorite ? !!data.user_ship.favorite : false;
  }

  show() {
    this.is_show = true;
  }

  hide() {
    this.is_show = false;
  }

  get healthLabel() {
    return `${(this.user_ship ? this.user_ship.health + ' / ' : '')} ${this.health}`;
  }

  get isPurchased(): boolean {
    return !!this.user_ship;
  }

  get isFavorite(): boolean {
    debugger;
    return !!this.is_favorite;
    // return !this.user_ship ? false : /*this.user_ship.is_favorite*/ true;
  }

  get isVisible() {
    return this.is_show;
  }

  get percentOfHealth(): number {
    return this.user_ship.health / this.health * 100;
  }

  get healthClass() {
    const percentOfHealth: number = this.percentOfHealth;
    if (percentOfHealth < 30) {
      return 'bg-danger'; // red
    } else if (percentOfHealth < 60) {
      return 'bg-warning'; // yellow
    }
    return 'bg-success'; // green
  }
}
