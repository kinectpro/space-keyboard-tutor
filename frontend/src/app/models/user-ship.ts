export class UserShip {
  readonly name: string;
  readonly health: number;
  readonly repair_start_at: Date;
  readonly repair_finish_at: Date;
}
