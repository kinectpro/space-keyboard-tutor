import {Rating} from './rating';

export interface StatisticInterface {
  readonly day: Rating[];
  readonly week: Rating[];
  readonly month: Rating[];
  readonly all: Rating[];
}

export class Statistic implements StatisticInterface {
  readonly day: Rating[];
  readonly week: Rating[];
  readonly month: Rating[];
  readonly all: Rating[];

  constructor(data: StatisticInterface) {
    this.day = data.day;
    this.week = data.week;
    this.month = data.month;
    this.all = data.all;
  }
}
