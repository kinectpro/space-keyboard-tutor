import {state} from '../enums';

export interface ShipInterface {
  readonly name: string;
  readonly model_id: number;
  readonly nickname: string;
  readonly id: number;
  readonly level: number;
  readonly user_id: number;
  readonly maxFuel: number;
  readonly healthMax: number;
  health: number;
  fuel: number;
  raceProgress: number;
  refuelingProgress: number;
  state: string;
}

export class UserShip implements ShipInterface {
  readonly name: string;
  readonly model_id: number;
  readonly nickname: string;
  readonly id: number;
  readonly level: number;
  readonly user_id: number;
  readonly maxFuel: number;
  readonly healthMax: number;
  health: number;
  fuel: number;
  raceProgress: number;
  refuelingProgress: number;
  state: string;
  animateState;

  constructor(data: ShipInterface) {
    this.name = data.name;
    this.model_id = data.model_id;
    this.nickname = data.nickname;
    this.id = data.id;
    this.level = data.level;
    this.user_id = data.user_id;
    this.health = data.health;
    this.maxFuel = data.maxFuel;
    this.fuel = data.fuel;
    this.raceProgress = data.raceProgress;
    this.refuelingProgress = data.refuelingProgress;
    this.state = data.state;
    this.animateState = state.move;
    this.healthMax = data.healthMax;
  }

  updateShip(data) {
    this.health = data.health;
    this.raceProgress = data.raceProgress;
    this.animateState = state.move;
  }

  animateValue(traceLen) {
    const currentPosition = this.raceProgress / traceLen * 100;
    return {
      value: this.animateState,
      params: {
        currentPosition: currentPosition <= 100 ? currentPosition : 100,
      }
    };
  }
}
