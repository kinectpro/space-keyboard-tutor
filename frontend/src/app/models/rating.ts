export interface RatingInterface {
  readonly id: number;
  readonly place: number;
  readonly nickname: string;
  readonly level: number;
  readonly time: number;
  readonly favorite: string;
  readonly gold: number;
  readonly silver: number;
  readonly wins: number;
  readonly exp: number;
}

export class Rating implements RatingInterface {
  readonly id: number;
  readonly place: number;
  readonly nickname: string;
  readonly level: number;
  readonly time: number;
  readonly favorite: string;
  readonly gold: number;
  readonly silver: number;
  readonly wins: number;
  readonly exp: number;

  constructor(data: RatingInterface) {
    this.id = data.id;
    this.place = 0;
    this.nickname = data.nickname;
    this.level = data.level;
    this.time = data.time;
    this.favorite = data.favorite;
    this.gold = data.gold;
    this.silver = data.silver;
    this.wins = data.wins;
    this.exp = data.exp;
  }
}
