import {Action} from './action';

export interface Message {
  token: string;
  content?: any | null;
  action?: Action;
}
