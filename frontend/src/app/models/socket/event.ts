export enum Event {
  CONNECT = 'connection',
  DISCONNECT = 'disconnect',
  JOIN = 'join',
  WORD = 'word',
  RESULT = 'result',
  START = 'start',
  GET_WORD = 'get word',
  UPDATE = 'update room',
  TIME_TO_START = 'time to start',
  RESULTS = 'results',
  NEW_PLAYER = 'new player',
  DESTROY = 'destroy',
  NO_USER_SHIP = 'noUserShip'
}
