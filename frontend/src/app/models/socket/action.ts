export enum Action {
  JOINED = 'join',
  LEFT = 'left',
  RENAME = 'rename'
}