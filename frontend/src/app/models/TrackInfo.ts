export interface CurrentShipInterface {
  readonly userId: number;
  readonly shipId: number;
}

export class TrackInfo implements CurrentShipInterface {
  readonly userId: number;
  readonly shipId: number;

  constructor(data: CurrentShipInterface) {
    this.userId = data.userId;
    this.shipId = data.shipId;
  }
}
