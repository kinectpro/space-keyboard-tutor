import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faCoffee} from '@fortawesome/free-solid-svg-icons';
import {RouterModule} from '@angular/router';
import {AnimatedBackgroundComponent} from './animated-background/animated-background.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    AnimatedBackgroundComponent
  ],
  exports: [
    HeaderComponent,
    AnimatedBackgroundComponent
  ]
})
export class SharedModule {
  constructor() {
    library.add(faCoffee);
  }
}
