import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {faCoins} from '@fortawesome/free-solid-svg-icons';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';

// Add an icon to the library for convenient access in other components
library.add(faCoins, faPlus);

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() user: User;

  constructor(private router: Router) { }
  faCoins = faCoins;

  ngOnInit() {
  }

  toHangar() {
    this.router.navigate(['dock']);
  }

}
