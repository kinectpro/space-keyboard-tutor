import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared-modules/shared.module';
import {ScoreRoutingModule} from './score-routing.module';
import {ScoreComponent} from './score.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    ScoreComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ScoreRoutingModule,
    FormsModule,
  ]
})
export class ScoreModule { }