import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {User} from '../models/user';
import {Observable, forkJoin} from 'rxjs';
import {Rating} from '../models/rating';
import {Statistic} from '../models/statistic';
import {enConstNum, enPeriod} from '../enums';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {
  user$: Observable<User> = this.userService.profile();
  user: User;

  statistic$: Observable<any> = this.userService.statistic();
  statistic: Statistic;

  currentPeriodStatistic: Rating[];
  period: string = enPeriod.DAY;
  currentUserStatistic: Rating;
  currentUserPlace: number = 0;

  error: any;
  enConstNum = enConstNum;

  constructor(private router: Router, private userService: UserService) {
    forkJoin(this.user$, this.statistic$)
      .subscribe(([user, statistic]) => {
        this.statistic = statistic;
        this.user = user;
        this.onChange(this.period);
      }, error => this.error = error);
  }

  ngOnInit() {
  }

  onChange(period) {
    this.period = period;
    this.currentPeriodStatistic = this.getStatistic(period).splice(0, enConstNum.RATING_COUNT);
    this.currentUserStatistic = this.currentUserStats;
    if (this.currentUserStatistic) {
      this.currentUserPlace = this.currentUserStatistic.id;
    }
  }

  getStatistic(period): Rating[] {
    switch (period) {
      case (enPeriod.WEEK):
        return Object.values(this.statistic.week);
      case (enPeriod.MONTH):
        return Object.values(this.statistic.month);
      case (enPeriod.ALL):
        return Object.values(this.statistic.all);
      default:
        return Object.values(this.statistic.day);
    }
  }

  get currentUserStats(): Rating {
    return this.getStatistic(this.period).find(el => {
      return el.id === this.user.id;
    });
  }
}
