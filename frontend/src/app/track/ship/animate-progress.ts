import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';

export const moveAnimation = [
  trigger('moveAnimation', [
    state('move', style({
      marginLeft: '{{currentPosition}}%',
    }), {params: {currentPosition: 0}}),
    state('stop', style({
      marginLeft: '{{currentPosition}}%',
    }), {params: {currentPosition: 0}}),

    state('start', style({
      marginLeft: '{{currentPosition}}%',
    }), {params: {currentPosition: 0}}),
    state('finish', style({'height': '0px'})),

    transition('* => *', animate(2000)),
  ]),
];

