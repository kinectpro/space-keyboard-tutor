import {animate, state, style, transition, trigger} from '@angular/animations';

export const refueling = [
  trigger('refueling', [
    state('refuel', style({
      left: '22px',
    }), {params: {currentPosition: 0}}),
    state('race', style({
      left: '-10px',
    })),

    transition('* => *', animate(1000)),
  ]),
];
