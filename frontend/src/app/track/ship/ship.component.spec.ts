import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShipComponent} from './ship.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('ShipComponent', () => {
  let component: ShipComponent;
  let fixture: ComponentFixture<ShipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShipComponent],
      imports: [BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
