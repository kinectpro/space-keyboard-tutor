import {Component, Input, OnInit} from '@angular/core';
import {UserShip} from '../../models/userShip';
import {moveAnimation} from './animate-progress';
import {refueling} from './animate-refueling';
import {state} from '../../enums';

@Component({
  selector: 'app-ship',
  templateUrl: './ship.component.html',
  styleUrls: ['./ship.component.scss'],
  animations: [
    moveAnimation,
    refueling
  ]
})
export class ShipComponent implements OnInit {
  @Input() ship: UserShip;
  @Input() level: number;
  @Input() traceLen: number;
  animatePositionValue;
  state = state.start;
  refuelState = 'refuel';

  constructor() {
  }

  ngOnInit() {
    this.animatePositionValue = {
      value: this.ship.animateState,
      params: {
        currentPosition: this.ship.raceProgress / this.traceLen * 100,
      }
    };
    // this.animatePositionValue = this.ship.getRaceProgress();
    setTimeout(() => {
      this.refuelState = state.race;
    }, 3000);
  }

  getAnimatePositionValue() {
    return this.ship.animateValue(this.traceLen);
  }

  animStart(event) {
    this.ship.animateState = state.stop;
  }

  get getPercentHealth() {
    return this.ship.health / this.ship.healthMax * 100;
  }
}
