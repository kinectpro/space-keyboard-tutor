import {animate, state, style, transition, trigger} from '@angular/animations';

export const disappearance = [
  trigger('disappearance', [
    state('show', style({
      display: 'block',
    }), {params: {currentPosition: 0}}),
    state('hide', style({
      display: 'none',
    })),

    transition('* => *', animate(1000)),
  ]),
];

