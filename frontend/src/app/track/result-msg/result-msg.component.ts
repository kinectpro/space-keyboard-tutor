import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-result-msg',
  templateUrl: './result-msg.component.html',
  styleUrls: ['./result-msg.component.css']
})
export class ResultMsgComponent implements OnInit {
  private place: number;
  private silver: number;
  private gold: number;
  private exp: number;
  private places = ['1st', '2nd', '3rd', '4th'];

  constructor(public bsModalRef: BsModalRef, private router: Router) {
  }

  ngOnInit() {
  }

  toHangar() {
    this.router.navigate(['dock']);
  }
}
