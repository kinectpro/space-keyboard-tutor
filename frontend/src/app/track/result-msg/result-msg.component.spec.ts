import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ResultMsgComponent} from './result-msg.component';

describe('ResultMsgComponent', () => {
  let component: ResultMsgComponent;
  let fixture: ComponentFixture<ResultMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResultMsgComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
