import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-words',
    templateUrl: './words.component.html',
    styleUrls: ['./words.component.scss']
})
export class WordsComponent implements OnInit {
    @Input() inputWord: string;
    @Output() inputTextChange: EventEmitter<string> = new EventEmitter();
    @Input() word: string;
    @Input() prevWord: string;
    @Input() nextWord: string;

    constructor() {
    }

    ngOnInit() {
    }

    keyUpFunction(event) {
        if (!this.inputWord) {
            return;
        }
        if (this.inputWord === this.word || this.inputWord.length === this.word.length) {
            this.inputTextChange.emit(this.inputWord);
            this.inputWord = null;
        } else if (event.keyCode === 13) {
            this.inputTextChange.emit(this.inputWord);
            this.inputWord = null;
        }
    }
}
