import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {Router} from "@angular/router";

@Component({
  selector: 'app-time-to-start',
  templateUrl: './time-to-start.component.html',
  styleUrls: ['./time-to-start.component.css']
})

export class TimeToStartComponent implements OnInit {
  time: number;

  constructor(public bsModalRef: BsModalRef, private router: Router) {
  }

  ngOnInit() {
    const timer = setInterval(() => {
      if (--this.time <= 0) {
        this.bsModalRef.hide();
        clearInterval(timer);
      }

    }, 1000);
  }

  toHangar() {
    this.router.navigate(['dock']);
  }
}
