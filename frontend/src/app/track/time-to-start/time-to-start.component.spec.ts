import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TimeToStartComponent} from './time-to-start.component';

describe('TimeToStartComponent', () => {
  let component: TimeToStartComponent;
  let fixture: ComponentFixture<TimeToStartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimeToStartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeToStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
