import {Component, OnInit, OnDestroy} from '@angular/core';
import {Message} from '../models/socket/message';
import {Event} from '../models/socket/event';
import {SocketService} from '../services/socket.service';
import {User} from '../models/user';
import {TrackInfoService} from '../services/track-info.service';
import {Router} from '@angular/router';
import {UserShip} from '../models/userShip';
import {UserService} from '../services/user.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TimeToStartComponent} from './time-to-start/time-to-start.component';
import {DestroyMsgComponent} from './destroy-msg/destroy-msg.component';
import {ResultMsgComponent} from './result-msg/result-msg.component';
import {state} from '../enums';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
  providers: [SocketService],
})

export class TrackComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  user: User;
  info: Array<string> = [];
  ships = [];
  word: string = null;
  prevWord: string = null;
  nextWord: string = null;
  result: string;
  inputWord: string;
  timeToStart = 0;
  loaded = false;
  trackLoad = false;
  isDisconnect = false;
  level: number;
  traceLen: number;
  trackInfo: {};
  isFinish = false;
  currentUserId: number;
  currentShipId: number;
  error: any;
  bsModalRef: BsModalRef;
  bsModalRefDestroy: BsModalRef;
  bsModalRefResult: BsModalRef;

  constructor(
    private socket: SocketService,
    private trackInfoService: TrackInfoService,
    private router: Router,
    private userService: UserService,
    private modalService: BsModalService,
  ) {
  }

  ngOnInit() {
    this.loadUser();
    this.trackInfo = this.trackInfoService.trackInfo;
    this.socket.emit(Event.JOIN, this.trackInfo)
      .subscribe(
        data => {
          if (data.timeToStart >= 0) {
            this.openModalWithComponent(data.timeToStart);
          }
          this.level = data.level;
          this.traceLen = data.traceLen;
          this.currentUserId = data.currentUserId;
          this.currentShipId = data.currentShipId;
          for (const ship of data.ships) {
            this.ships.push(new UserShip(ship));
          }
        }, error => {
          console.log('Error', error);
        }
      );

    this.socket.on(Event.NEW_PLAYER)
      .subscribe(data => {
          console.log(data);
          this.ships.push(new UserShip(data));
        }, error => {
          console.log('Error', error);
        }
      );

    this.socket.on(Event.UPDATE)
      .subscribe(data => {
        for (const inputShip of data) {
          if (!this.bsModalRefDestroy && inputShip.user_id === this.currentUserId && inputShip.state === state.destroy) {
            this.bsModalRefDestroy = this.modalService.show(DestroyMsgComponent, {backdrop: 'static'});
          }
          for (const ship of this.ships) {
            if (ship.id === inputShip.id) {
              ship.updateShip(inputShip);
            }
          }
        }
      });

    this.socket.on(Event.DISCONNECT)
      .subscribe(() => {
        this.disconnect();
      });

    this.socket.on(Event.START)
      .subscribe(() => {
        this.trackLoad = true;
        this.getWord();
      });

    this.socket.on(Event.NO_USER_SHIP)
      .subscribe(() => {
        this.router.navigate(['dock']);
      });
  }

  getWord() {
    this.socket.emit(Event.GET_WORD)
      .subscribe(data => {
          if (data.isFinish) {
            this.isFinish = true;
            return;
          }
          this.word = data.word;
          this.prevWord = data.prevWord;
          this.nextWord = data.nextWord;
        }, error => {
          console.log('Error', error);
        }
      );
  }

  sendInputWord(inputWord) {
    this.socket.emit(Event.WORD, {word: inputWord})
      .subscribe(data => {
          this.result = data.result;
          this.inputWord = null;
          const initialState = {
            place: data.place,
            silver: data.silver,
            gold: data.gold,
            exp: data.exp
          };
          if (data.isFinish) {
            console.log(data.ships);
            this.bsModalRefResult = this.modalService.show(ResultMsgComponent, {initialState, backdrop: 'static'});
            return;
          }
        }, error => {
          console.log('Error', error);
        }, () => {
          this.getWord();
        }
      );
  }

  disconnect() {
    this.socket.disconnect();
  }

  loadUser() {
    this.userService.profile()
      .subscribe((user: User) => {
        this.user = user;
        localStorage.setItem('userId', user.id.toString());
      }, error => {
        this.error = error.error.error;
      });
  }

  openModalWithComponent(timeTostart) {
    const initialState = {
      time: timeTostart
    };
    this.bsModalRef = this.modalService.show(TimeToStartComponent, {initialState, backdrop: 'static'});
  }

  ngOnDestroy() {
    this.socket.on(Event.DISCONNECT);
    if (this.bsModalRef) {
      this.bsModalRef.hide();
    }
    if (this.bsModalRefDestroy) {
      this.bsModalRefDestroy.hide();
    }
    if (this.bsModalRefResult) {
      this.bsModalRefResult.hide();
    }
  }
}
