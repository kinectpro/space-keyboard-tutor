import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DestroyMsgComponent} from './destroy-msg.component';

describe('DestroyMsgComponent', () => {
  let component: DestroyMsgComponent;
  let fixture: ComponentFixture<DestroyMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DestroyMsgComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestroyMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
