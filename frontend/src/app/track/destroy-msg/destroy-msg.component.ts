import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-destroy-msg',
  templateUrl: './destroy-msg.component.html',
  styleUrls: ['./destroy-msg.component.css']
})
export class DestroyMsgComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef, private router: Router) {

  }

  ngOnInit() {
  }

  toHangar() {
    this.router.navigate(['dock']);
  }
}
