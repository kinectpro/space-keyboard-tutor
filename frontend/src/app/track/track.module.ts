import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrackRoutingModule} from './track-routing.module';
import {TrackComponent} from './track.component';
import {ShipComponent} from './ship/ship.component';
import {FormsModule} from '@angular/forms';
import {WordsComponent} from './words/words.component';
import {SharedModule} from '../shared-modules/shared.module';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TimeToStartComponent} from './time-to-start/time-to-start.component';
import {DestroyMsgComponent} from './destroy-msg/destroy-msg.component';
import {ResultMsgComponent} from './result-msg/result-msg.component';

@NgModule({
  declarations: [
    TrackComponent,
    ShipComponent,
    WordsComponent,
    TimeToStartComponent,
    DestroyMsgComponent,
    ResultMsgComponent,
  ],
  imports: [
    CommonModule,
    TrackRoutingModule,
    FormsModule,
    SharedModule,
    ModalModule.forRoot(),
  ],
  entryComponents: [
    TimeToStartComponent,
    DestroyMsgComponent,
    ResultMsgComponent
  ]
})
export class TrackModule {
}
