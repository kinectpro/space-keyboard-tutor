import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as io from 'socket.io-client';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class SocketService {

  options = {
    headers: new HttpHeaders({
      Authorization: 'Bearer' + ' ' + localStorage.getItem('token'),
    })
  };
  private host = 'http://localhost:3000';
  private socket: any;

  constructor() {
    this.socket = io.connect('http://localhost:3000', {
      'query': 'token=' + localStorage.getItem('token')
    });
    this.socket.on('disconnect', () => this.disconnected());
    this.socket.on('error', (error: string) => {
      console.log(`ERROR: "${error}" (${this.host})`);
      console.log(error);
    });
    this.socket.on('unauthorized', function (error, callback) {
      if (error.data.type === 'UnauthorizedError' || error.data.code === 'invalid_token') {
        this.router.navigate(['login']);
        callback();
        console.log('User\'s token has expired');
      }
    });
    this.socket.on('shipEmpty', function (error, callback) {
      this.router.navigate(['dock']);
      callback();
      console.log('ship empty');
    });

  }

  connect() {
    this.socket.connect();
  }

  disconnect() {
    this.socket.disconnect();
  }

  emit(chanel, message = null) {
    return new Observable<any>(observer => {
      if (chanel === 'word')
        observer.next(true);
      // console.log(`emit to ${chanel}:`, message);
      this.socket.emit(chanel, message, function (data) {
        // console.log(data);
        if (data.success) {
          observer.next(data.msg);
        } else {
          observer.error(data.msg);
        }
        observer.complete();
      });
    });
  }

  on(event_name) {
    console.log(`listen to ${event_name}:`);
    return new Observable<any>(observer => {
      this.socket.off(event_name);
      this.socket.on(event_name, (data) => {
        observer.next(data);
      });
    });
  }

  private connected() {
    console.log('Connected');
  }

  private disconnected() {
    console.log('Disconnected');
  }
}
