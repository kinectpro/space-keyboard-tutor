import {TestBed} from '@angular/core/testing';

import {DockService} from './dock.service';

describe('DockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DockService = TestBed.get(DockService);
    expect(service).toBeTruthy();
  });
});
