import {Injectable} from '@angular/core';
import {TrackInfo} from '../models/TrackInfo';

@Injectable({
  providedIn: 'root'
})
export class TrackInfoService {
  trackInfo: TrackInfo;

  constructor() {
  }

  get info() {
    return this.trackInfo;
  }

  set info(trackInfo) {
    this.trackInfo = trackInfo;
  }
}
