import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = environment.apiUrl + '/auth/';

  constructor(private http: HttpClient) {
  }

  login(data) {
    return this.http.post<any>(this.url + 'login', data);
  }

  logout() {
    localStorage.removeItem('token');
  }

  register(data) {
    return this.http.post(this.url + 'register', data);
  }

  confirm(token) {
    return this.http.post(this.url + 'confirm', {token: token});
  }

  isAuthenticated(): boolean {
    const helper = new JwtHelperService();
    const token = localStorage.getItem('token');
    return !helper.isTokenExpired(token);
  }

  saveToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  forgotPassword(data) {
    return this.http.post(this.url + 'forgot-password', data);
  }

  validatePasswordToken(token) {
    return this.http.post(this.url + 'validate-password-token', {token: token});
  }

  changePassword(data) {
    return this.http.post(this.url + 'reset-password', data);
  }
}
