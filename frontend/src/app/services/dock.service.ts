import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DockService {

  url = environment.apiUrl + '/dock';
  options = {
    headers: new HttpHeaders({
      Authorization: 'Bearer' + ' ' + localStorage.getItem('token'),
    })
  };

  constructor(private http: HttpClient) {
  }

  show() {
    return this.http.post(this.url, {}, this.options);
  }

  toRepair(userShipId) {
    return this.http.put(this.url + '/to-repair/' + userShipId, {}, this.options);
  }

  fromRepair(userShipId) {
    return this.http.put(this.url + '/from-repair/' + userShipId, {}, this.options);
  }

  addToFavorite(userShipId) {
    return this.http.put(this.url + '/add-to-favorite/' + userShipId, {}, this.options);
  }

  removeFromFavorite(userShipId) {
    return this.http.put(this.url + '/remove-from-favorite/' + userShipId, {}, this.options);
  }

  getHealthStatus(userShipId) {
    return this.http.put(this.url + '/get-health-status/' + userShipId, {}, this.options);
  }

  buyShip(shipId) {
    return this.http.put(this.url + '/buy-ship-silver/' + shipId, {}, this.options);
  }
}
