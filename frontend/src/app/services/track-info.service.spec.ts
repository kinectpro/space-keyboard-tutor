import {TestBed} from '@angular/core/testing';

import {TrackInfoService} from './track-info.service';

describe('TrackInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackInfoService = TestBed.get(TrackInfoService);
    expect(service).toBeTruthy();
  });
});
