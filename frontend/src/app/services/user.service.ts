import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = environment.apiUrl + '/profile';
  user;
  options = {
    headers: new HttpHeaders({
      Authorization: 'Bearer' + ' ' + this.auth.getToken(),
    })
  };

  constructor(private http: HttpClient, private auth: AuthService) {
    this.user = this.http.get(this.url + '', this.options);
  }

  profile() {
    return this.user;
  }

  editProfile(data) {
    return this.http.put(this.url + '/edit', data, this.options);
  }

  statistic() {
    return this.http.get(this.url + '/statistic', this.options);
    return this.user;
  }

  changePassword(data) {
    return this.http.put(this.url + '/change-password', data, this.options);
  }
}
