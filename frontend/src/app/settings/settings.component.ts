import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../services/user.service';
import {FormBuilder, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {PasswordValidator} from '../main/helpers/password-validator';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  error: any;
  user: User;
  editSuccess: boolean = false;
  changePasswordSuccess: boolean = false;
  formPassword = this.formBuilder.group({
    oldPassword: ['', [Validators.required, Validators.minLength(environment.passwordMinLength)]],
    password: ['', [Validators.required, Validators.minLength(environment.passwordMinLength)]],
    confirmPassword: ['', Validators.required]
  }, {
    validator: PasswordValidator.validate.bind(this)
  });

  formProfile = this.formBuilder.group({
    nickname: ['', Validators.compose([Validators.required, Validators.minLength(5)])]

  });

  get nickname() {
    return this.formProfile.get('nickname');
  }

  get password() {
    return this.formPassword.get('password');
  }

  get confirmPassword() {
    return this.formPassword.get('confirmPassword');
  }

  get oldPassword() {
    return this.formPassword.get('oldPassword');
  }

  constructor(private userService: UserService, private formBuilder: FormBuilder) {
    this.loadUser();
  }

  ngOnInit() {
  }

  loadUser() {
    this.userService.profile()
      .subscribe((user: User) => {
        this.user = user;
        localStorage.setItem('userId', user.id.toString());
      }, error => {
        this.error = error.error.error;
      });
  }

  onSubmitEditProfile() {
    this.userService.editProfile(this.formProfile.value)
      .subscribe(() => {
        this.editSuccess = true;
      }, error => {
        this.error = error.error.email;
      });
  }

  onSubmitChangePassword() {
    this.userService.changePassword(this.formPassword.value)
      .subscribe(() => {
        this.changePasswordSuccess = true;
      }, error => {
        this.error = error.error.email;
      });
  }
}
