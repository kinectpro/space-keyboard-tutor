import {Pipe, PipeTransform} from '@angular/core';
import {Ship} from '../../models/ship';
import {enShipFilter} from '../../enums';

@Pipe({
  name: 'shipsFilter',
  pure: false
})
export class ShipsFilterPipe implements PipeTransform {
  transform(ships: any, args?: any): any {
    return ships.filter((ship: Ship) => {
      if (args === enShipFilter.favorite) {
        return ship.isFavorite;
      } else if (args === enShipFilter.available) {
        return ship.is_enabled;
      } else if (args === enShipFilter.inHangar) {
        return ship.isPurchased;
      }
      return true;
    });
  }
}
