import {Pipe, PipeTransform} from '@angular/core';
import {Ship} from '../../models/ship';
import {enShipOrder} from '../../enums';

@Pipe({
  name: 'shipsOrder'
})
export class ShipsOrderPipe implements PipeTransform {
  transform(ships: Array<Ship>, args?: any): any {
    return ships.sort((a: Ship, b: Ship) => {
      return args === enShipOrder.levelAsc ?
        (a.level === b.level) ? 0 : ((b.level > a.level) ? 1 : -1) :
        (a.level === b.level) ? 0 : ((b.level < a.level) ? 1 : -1);
    });
  }

}
