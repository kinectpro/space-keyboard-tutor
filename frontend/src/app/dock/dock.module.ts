import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DockRoutingModule} from './dock-routing.module';
import {DockComponent} from './dock.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ShipComponent} from './ship/ship.component';
import {SharedModule} from '../shared-modules/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from '@angular/forms';
import {ShipsFilterPipe} from './pipes/ships-filter.pipe';
import {ShipsOrderPipe} from './pipes/ships-order.pipe';

@NgModule({
  imports: [
    CommonModule,
    DockRoutingModule,
    NgxSpinnerModule,
    SharedModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DockComponent,
    ShipComponent,
    ShipsFilterPipe,
    ShipsOrderPipe
  ]
})
export class DockModule {
}
