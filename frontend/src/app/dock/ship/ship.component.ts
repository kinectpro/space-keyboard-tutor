import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {Ship} from '../../models/ship';
import {NgxSpinnerService} from 'ngx-spinner';
import {DockService} from '../../services/dock.service';
import {finalize} from 'rxjs/operators';
import {Router} from '@angular/router';
import {TrackInfoService} from '../../services/track-info.service';

@Component({
    selector: 'app-ship',
    templateUrl: './ship.component.html',
    styleUrls: ['./ship.component.scss'],
})
export class ShipComponent implements OnInit {

    imgUrlDefault = '/assets/images/default.png';

    constructor(
        private spinner: NgxSpinnerService,
        private dock: DockService,
        private router: Router,
        private trackInfo: TrackInfoService
    ) {
    }

    @Input() ship: Ship;
    @Output() update = new EventEmitter<boolean>();
    @Output() error = new EventEmitter<string>();

    ngOnInit() {
        if (this.ship.user_ship && this.ship.user_ship.repair_start_at) {
            console.log('this.ship', this.ship);
            this.updateHealthLine();
        }
    }

    toRepair() {
        this.spinner.show();
        this.dock.toRepair(this.ship.user_ship.id)
            .subscribe(() => {
                this.ship.user_ship.repair_start_at = new Date();
                this.update.emit();
                this.spinner.hide();
                this.updateHealthLine();
            }, error => {
                this.error.emit(error.error.error);
            });
    }

    updateHealthLine() {
        // TODO нужно оптимизировать запрос на статус здоровья корабля, чтобы был один запрос на все юзерские корабли, а не на каждый отдельно
        this.ship.user_ship.updateInterval = setInterval(() => {
                this.dock.getHealthStatus(this.ship.user_ship.id)
                    .subscribe((d: any) => {
                        this.ship.user_ship.health = d.ship.health;

                        if (this.ship.user_ship.health === this.ship.health) {
                            this.stopUpdateHealthLine();
                        }
                    }, error => {
                        this.error.emit(error.error.error);
                    });

            }, 5000
        );
    }

    stopUpdateHealthLine() {
        clearInterval(this.ship.user_ship.updateInterval);
        this.ship.user_ship.repair_start_at = null;
    }

    fromRepair() {
        this.stopUpdateHealthLine();
        this.spinner.show();
        this.dock.fromRepair(this.ship.user_ship.id)
            .subscribe((data: any) => {
                this.ship.user_ship.repair_start_at = null;
                this.update.emit();
                this.spinner.hide();
                this.ship.user_ship.health = data.ship.health;
            }, error => {
                this.error.emit(error.error.error);
            });
    }

    buyShip(ship) {
        this.spinner.show();
        this.dock.buyShip(ship.id).pipe(finalize(() => this.spinner.hide()))
            .subscribe(() => {
                this.update.emit();
            }, error => {
                this.error.emit(error.error.error);
            });
    }

    toGame(ship) {
        this.trackInfo.info = {
            shipId: ship.user_ship.ship_model_id,
            userId: ship.user_ship.user_id
        };

        this.router.navigate(['track']);
    }

    addToFavorite() {
        this.ship.user_ship.favorite = this.ship.is_favorite = true;
        this.dock.addToFavorite(this.ship.user_ship.id).subscribe(
            () => {
                this.update.emit();
            },
            error => {
                this.error.emit(error.error.error);
            });
    }

    removeFromFavorite() {
        this.ship.user_ship.favorite = this.ship.is_favorite = false;
        this.dock.removeFromFavorite(this.ship.user_ship.id).subscribe(
            () => {
                this.update.emit();
            },
            error => {
                this.error.emit(error.error.error);
            });
    }
}
