import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {DockService} from '../services/dock.service';
import {Ship} from '../models/ship';
import {finalize, map} from 'rxjs/operators';
import {User} from '../models/user';
import {UserService} from '../services/user.service';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faCaretUp} from '@fortawesome/free-solid-svg-icons';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {Observable} from 'rxjs/Observable';
import {ShipsFilterPipe} from './pipes/ships-filter.pipe';
import {enShipFilter, enShipOrder} from '../enums';

// Add an icon to the library for convenient access in other components
library.add(faCaretUp, faCaretDown);

@Component({
  selector: 'app-dock',
  templateUrl: './dock.component.html',
  styleUrls: ['./dock.component.scss'],
  providers: [ShipsFilterPipe],
})
export class DockComponent implements OnInit {
  filter: Observable<string>;

  loaded: boolean = false;
  ships: Array<Ship> | null = null;

  enShipsFilter = enShipFilter;
  shipFilter = this.enShipsFilter.all;
  enShipsOrder = enShipOrder;
  shipOrder = this.enShipsOrder.levelAsc;

  shipFilterStorageKey = 'shipsFilter';
  shipOrderStorageKey = 'shipsOrder';

  error: string;
  user: User;
  renderer;

  constructor(private router: Router,
              private spinner: NgxSpinnerService,
              private userService: UserService,
              private shipsFilterPipe: ShipsFilterPipe,
              private dockService: DockService
  ) {
  }

  ngOnInit() {
    if (localStorage.getItem(this.shipFilterStorageKey)) {
      this.shipFilter = localStorage.getItem(this.shipFilterStorageKey) as enShipFilter;
    }

    this.loadUser();
    this.loadShips();
  }

  loadShips() {
    this.spinner.show();
    this.dockService.show()
      .pipe(finalize(() => this.spinner.hide()))
      .subscribe((ships: Array<any>) => {
        this.ships = ships.map(item => new Ship(item));
        this.loaded = true;
        console.log(this.ships);
      }, error => {
        this.error = error.error.error;
      });

  }

  loadUser() {
    this.user = this.userService.profile()
      .subscribe((user: User) => {
        this.user = user;
        localStorage.setItem('userId', user.id.toString());
      }, error => {
        this.error = error.error.error;
      });
  }

  getError(error: string) {
    this.error = error;
  }

  changeFilter(filter: enShipFilter) {
    this.shipFilter = filter;
    localStorage.setItem(this.shipFilterStorageKey, this.shipFilter);
  }

  switchSortOrder() {
    this.shipOrder = this.shipOrder === this.enShipsOrder.levelDesc ? this.enShipsOrder.levelAsc : this.enShipsOrder.levelDesc;
    localStorage.setItem(this.shipOrderStorageKey, this.shipOrder);
  }
}
